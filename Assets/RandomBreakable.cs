﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class RandomBreakable : MonoBehaviour
{
    public GameObject caisse1;
    public GameObject caisse2;
    public GameObject pot1;
    public GameObject pot2;
    
    void Start()
    {
        int randN = Random.Range(0,3);

        switch (randN)
        {
            case 0 :
                Instantiate(caisse1, transform.position, Quaternion.identity);
                break;
            case 1 :
                Instantiate(caisse2, transform.position, Quaternion.identity);
                break;
            case 2 :
                Instantiate(pot1, transform.position, Quaternion.identity);
                break;
            case 3 :
                Instantiate(pot2, transform.position, Quaternion.identity);
                break;
        }
        Destroy(gameObject);
    }
}
