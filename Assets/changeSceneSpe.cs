﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class changeSceneSpe : MonoBehaviour
{
    private GameObject gameManager;

    private void Start()
    {
        gameManager = GameObject.Find("Map");
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
            gameManager.GetComponent<Restart>().GoDunegeon();
    }
}
