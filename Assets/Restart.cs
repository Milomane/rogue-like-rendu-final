﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Restart : MonoBehaviour
{
    public GameObject dontdestroy;
    
    void Start()
    {
        dontdestroy = GameObject.Find("Don't Destroy");
    }

    public void DestroyDoubleRestart()
    {
        dontdestroy.GetComponent<DontDestroy>().SetFalse();
        Destroy(dontdestroy);
        SceneManager.LoadScene("Lobby");
    }
    
    public void DestroyDoubleMenu()
    {
        dontdestroy.GetComponent<DontDestroy>().SetFalse();
        Destroy(dontdestroy);
        SceneManager.LoadScene("Menu");
    }

    public void GoDunegeon()
    {
        dontdestroy.GetComponent<DontDestroy>().SetFalse();
        Destroy(dontdestroy);
        SceneManager.LoadScene("Dungeon");
    }
    
    public void Go(string sceneToGo)
    {
        dontdestroy.GetComponent<DontDestroy>().SetFalse();
        Destroy(dontdestroy);
        SceneManager.LoadScene(sceneToGo);
    }
}
