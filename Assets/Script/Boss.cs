﻿using System;
using System.Collections;
using System.Collections.Generic;
using AudioAutoPlayerSpace;
using DG.Tweening;
using Pathfinding;
using UnityEngine;
using UnityEngine.Audio;

public class Boss : MonoBehaviour
{
    public GameObject prefabBullet;
    public int bulletCount = 7;
    public float angleBullet = 160;

    public float speed = 400f;
    public float attackTimer = 5;
    private float attackCooldown;
    private float angle;

    public float cacDistance;

    public int health;
    public Color healthBarColor1;
    public Color healthBarColor2;
    private int maxHealth;
    private bool dead;

    public AudioClip hurtClip;
    public AudioClip deathClip;
    public AudioClip attackClip;
    public AudioClip VictoryClip;
    public AudioMixerGroup ennemyMixerGroup;

    public GameObject attackCollidersUp;
    public GameObject attackCollidersDown;
    public GameObject attackCollidersLeft;
    public GameObject attackCollidersRight;
    
    [Header("Seeker")]
    public float nextWaypointDistance = .05f;

    private Path path;
    private int waypoint = 0;
    private bool finishPath = false;
    private Rigidbody2D rb;
    private Seeker seeker;
    private Animator animator;
    private CameraPivotScript cameraPivot;
    private CanvasVar canvasVar;

    private PlayerController player;

 //   public GameObject victoryPanel;

    void Start()
    {
        player = FindObjectOfType<PlayerController>();
        rb = GetComponent<Rigidbody2D>();
        seeker = GetComponent<Seeker>();
        animator = GetComponent<Animator>();
        canvasVar = FindObjectOfType<CanvasVar>();
        cameraPivot = GameObject.FindGameObjectWithTag("CameraPivot").GetComponent<CameraPivotScript>();
        

        InvokeRepeating("UpdatePath", 0f, 0.5f);
        
        attackCooldown = attackTimer + 1;

        maxHealth = health;
        canvasVar.bossBar.SetActive(true);
        StartCoroutine(BossBarAnim());
    }
    void UpdatePath()
    {
        if (seeker.IsDone())
        {
            seeker.StartPath(rb.position, player.transform.position, PathCompleted);
        }
    }
    private void PathCompleted(Path p)
    {
        if (!p.error)
        {
            path = p;
            waypoint = 0;
        }
    }

    void Update()
    {
        if (health <= 0 && !dead)
        {
            dead = true;
            StopAllCoroutines();
            StartCoroutine(Death());
        }

        canvasVar.bossBarScaler.localScale = new Vector3((float)health / maxHealth, 1, 1);

        if (attackCooldown <= 0)
        {
            attackCooldown = attackTimer;
            GameObject colliderUsed;
            
            Vector2 vectorToTarget = player.transform.position - transform.position;
            angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
            
            if (Vector2.Distance(player.transform.position, transform.position) > cacDistance)
            {
                // Attack bullet
                Debug.Log("BULLET");
                StartCoroutine(BulletAttack());
            }
            else
            {
                // Attack cac
                StartCoroutine(SwordAttack());
            }
        }

        attackCooldown -= Time.deltaTime;
    } 
    
    void FixedUpdate()
    {
        if (!dead)
        {
            if (path == null)
            {
                return;
            }

            if (waypoint >= path.vectorPath.Count)
            {
                finishPath = true;
                return;
            }
            else
            {
                finishPath = false;
            }

            Vector2 direction = ((Vector2) path.vectorPath[waypoint] - rb.position).normalized;
            Vector2 force = direction * speed * Time.deltaTime;
        
            rb.AddForce(force);

            float distance = Vector2.Distance(rb.position, path.vectorPath[waypoint]);
            if (distance < nextWaypointDistance)
            {
                waypoint++;
            }
        
            if (direction.x != 0)
                animator.SetFloat("DirX", rb.velocity.x);
            if (direction.y != 0)
                animator.SetFloat("DirY", rb.velocity.y);
        }
    }

    public void PlayerTakeDamage()
    {
        player.GetComponent<PlayerHealth>().TakeDamage(1, 4, transform.position);
        attackCollidersDown.SetActive(false);
        attackCollidersLeft.SetActive(false);
        attackCollidersRight.SetActive(false);
        attackCollidersUp.SetActive(false);
    }

    public IEnumerator TakeDamage(int damage)
    {
        health -= damage;
        if (health < 0) health = 0;
        
        AudioAutoPlayer.Play(hurtClip, transform.position, ennemyMixerGroup, .2f);
        GetComponent<SpriteRenderer>().DOColor(Color.red, .2f);
        transform.DOScale(Vector3.one * .9f, .2f);
        
        yield return new WaitForSeconds(.2f);
        GetComponent<SpriteRenderer>().DOColor(Color.white, .2f);
        transform.DOScale(Vector3.one, .2f);
    }

    public IEnumerator SwordAttack()
    {
        GameObject collider = new GameObject();
        AudioAutoPlayer.Play(attackClip, transform.position, ennemyMixerGroup, .2f);
        animator.SetBool("Attack", true);
        
        yield return new WaitForSeconds(1.16f);
        if (angle > 45 && angle <= 135)
        {
            collider = attackCollidersUp;
        }
        if (angle > 135 || angle < -135)
        {
            collider = attackCollidersLeft;
        }
        if (angle < -45 && angle >= -135)
        {
            collider = attackCollidersDown;
        }
        if (angle >= -45 && angle <= 45)
        {
            collider = attackCollidersRight;
        }
        
        animator.SetBool("Attack", false);
        collider.SetActive(true);
        
        yield return new WaitForSeconds(.2f);
        collider.SetActive(false);
    }

    public IEnumerator BulletAttack()
    {
        AudioAutoPlayer.Play(attackClip, transform.position, ennemyMixerGroup, .2f);
        animator.SetBool("BAttack", true);
        
        yield return new WaitForSeconds(1);
        ShootBullet(0);
        yield return new WaitForSeconds(.3f);
        ShootBullet(0);
        
        yield return new WaitForSeconds(1);
        ShootBullet((angleBullet/bulletCount)/2);
        yield return new WaitForSeconds(.3f);
        ShootBullet((angleBullet/bulletCount)/2);
        
        animator.SetBool("BAttack", false);
    }

    public void ShootBullet(float angleModif)
    {
        for (int i = 0; i < bulletCount; i++)
        {
            float shootAngle;
            shootAngle = angle - (angleBullet/2) + (angleBullet/bulletCount)*i + angleModif;

            shootAngle = shootAngle * Mathf.Deg2Rad;

            Vector3 pos = new Vector3(Mathf.Cos(shootAngle)*2, Mathf.Sin(shootAngle)*2, 0) + transform.position;

            BossBullet bullet = Instantiate(prefabBullet, transform.position, Quaternion.identity).GetComponent<BossBullet>();
            bullet.shootVector = (pos - transform.position).normalized;
        }
    }

    public IEnumerator Death()
    {
        AudioAutoPlayer.Play(deathClip, transform.position, ennemyMixerGroup, .7f);
        player.playerAsControl = false;
        cameraPivot.SpecificTarget(gameObject);
        animator.SetBool("Dead", true);
        
        yield return new WaitForSeconds(.01f);
        yield return new WaitForSeconds(animator.GetCurrentAnimatorClipInfo(0).Length);
        
        canvasVar.bossBar.SetActive(false);

        canvasVar.victoryPanel.SetActive(true);
        AudioAutoPlayer.Play(VictoryClip, transform.position, ennemyMixerGroup, .5f);

        Destroy(gameObject);
    }

    public IEnumerator BossBarAnim()
    {
        canvasVar.bossBarImage.DOColor(healthBarColor1, 2f);
        yield return new WaitForSeconds(2f);
        canvasVar.bossBarImage.DOColor(healthBarColor2, 2f);
        yield return new WaitForSeconds(2f);
        StartCoroutine(BossBarAnim());
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        for (int i = 0; i < bulletCount; i++)
        {
            float shootAngle;
            shootAngle = angle - (angleBullet/2) + (angleBullet/bulletCount)*i;

            shootAngle = shootAngle * Mathf.Deg2Rad;

            Vector3 pos = new Vector3(Mathf.Cos(shootAngle)*2, Mathf.Sin(shootAngle)*2, 0) + transform.position;
            
            Gizmos.DrawWireSphere(pos,  1);
        }
    }
}
