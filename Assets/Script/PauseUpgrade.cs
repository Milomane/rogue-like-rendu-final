﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PauseUpgrade : MonoBehaviour
{
    public GameObject prefabBox;

    public GameObject descriptionObject;
    public TextMeshProUGUI descriptionText;
    public TextMeshProUGUI nameText;
    public Image itemSprite;
    
    public TextMeshProUGUI attack, speed, range, knockback, angle, stunTime, playerSpeed, playerDashDistance, playerHealth;

    public Color grayText, greenText, redText;

    public PlayerController playerController;
    private int itemCount;

    public float boxDistance;

    public int index;
    public List<GameObject> boxList;

    void Start()
    {
        playerController = FindObjectOfType<PlayerController>();
    }
    
    void Update()
    {
        playerController = FindObjectOfType<PlayerController>();

        itemCount = playerController.upgrades.Count;
        
        while (boxList.Count < itemCount)
        {
            boxList.Add(Instantiate(prefabBox, transform));
            int y = boxList.Count-1;
            boxList[y].transform.localPosition = new Vector3(0, -600, 0);
            boxList[y].GetComponent<PauseBoxScript>().indexButton.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(delegate { SetIndex(y); });
        }
        
        float indexAddPos = 0f;
        
        indexAddPos = -(itemCount * boxDistance)/2 + index * boxDistance;
        
        
        for (int i = 0; i < itemCount; i++)
        {
            boxList[i].GetComponent<PauseBoxScript>().targetPos = new Vector3(0, -boxDistance*i + (boxDistance*itemCount)/2 + indexAddPos,0);
            boxList[i].GetComponent<PauseBoxScript>().upgrade = playerController.upgrades[i];

            boxList[i].GetComponent<PauseBoxScript>().select = (i == index);
        }
        
        index += -(int)Input.mouseScrollDelta.y;

        if (index >= itemCount)
        {
            index = index - itemCount;
        } else if (index < 0)
        {
            index = itemCount + index;
        }

        if (boxList.Count > 0)
        {
            descriptionObject.SetActive(true);
            
            descriptionText.text = boxList[index].GetComponent<PauseBoxScript>().upgrade.itemDescription;
            nameText.text = boxList[index].GetComponent<PauseBoxScript>().upgrade.itemName;
            itemSprite.sprite = boxList[index].GetComponent<PauseBoxScript>().upgrade.sprite;

            SetStat(attack, boxList[index].GetComponent<PauseBoxScript>().upgrade.weaponUpgrade.attack);
            SetStat(speed, boxList[index].GetComponent<PauseBoxScript>().upgrade.weaponUpgrade.speed);
            SetStat(range, boxList[index].GetComponent<PauseBoxScript>().upgrade.weaponUpgrade.range);
            SetStat(knockback, boxList[index].GetComponent<PauseBoxScript>().upgrade.weaponUpgrade.knockback);
            SetStat(angle, boxList[index].GetComponent<PauseBoxScript>().upgrade.weaponUpgrade.angle);
            SetStat(stunTime, boxList[index].GetComponent<PauseBoxScript>().upgrade.weaponUpgrade.stuntime);
            
            SetStat(playerSpeed, boxList[index].GetComponent<PauseBoxScript>().upgrade.playerUpgrade.speed);
            SetStat(playerDashDistance, boxList[index].GetComponent<PauseBoxScript>().upgrade.playerUpgrade.dashDistance);
            SetStat(playerHealth, boxList[index].GetComponent<PauseBoxScript>().upgrade.playerUpgrade.health);
        }
        else
        {
            descriptionObject.SetActive(false);
        }
    }

    void SetStat(TextMeshProUGUI text, int stat)
    {
        if (stat == 0)
        {
            text.color = grayText;
            text.text = "0";
        } 
        else if (stat > 0)
        {
            text.color = greenText;
            text.text = "+" + stat;
        }
        else
        {
            text.color = redText;
            text.text = "-" + stat;
        }
    }

    void SetIndex(int newIndex)
    {
        Debug.Log(newIndex);
        
        index = newIndex;
    }
}
