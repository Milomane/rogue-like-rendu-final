﻿using System.Collections;
using System.Collections.Generic;
using GeneralClass;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{
    private PlayerHealth playerHealth;
    private PlayerMovement playerMovement;
    private PlayerController playerController;
    
    public int health;
    public int dashDistance;
    public int speed;

    private int baseHealth;
    private int baseDashDistance;
    private int baseSpeed;
    
    // Start is called before the first frame update
    void Start()
    {
        playerHealth = GetComponent<PlayerHealth>();
        playerMovement = GetComponent<PlayerMovement>();
        playerController = GetComponent<PlayerController>();

        baseHealth = health;
        baseDashDistance = dashDistance;
        baseSpeed = speed;
    }

    // Update is called once per frame
    void Update()
    {
        if (health < 0) health = 1;
        if (dashDistance < 0) dashDistance = 1;
        if (speed < 0) speed = 1;

        playerHealth.numOfHeart = HealthCalc(health);
        playerMovement.maxSpeed = SpeedCalc(speed);
        playerMovement.dashDistance = DashDistanceCalc(dashDistance);
    }
    
    public void ApplyStats()
    {
        health = baseHealth;
        dashDistance = baseDashDistance;
        speed = baseSpeed;

        foreach (var upgrade in playerController.upgrades)
        {
            health += upgrade.playerUpgrade.health;
            dashDistance += upgrade.playerUpgrade.dashDistance;
            speed += upgrade.playerUpgrade.speed;
        }
    }
    
    int HealthCalc(int value)
    {
        int vHealth = value;

        return vHealth;
    }
    
    float DashDistanceCalc(int value)
    {
        float vDashDistance = (float)value / 6;

        return vDashDistance;
    }
    
    float SpeedCalc(int value)
    {
        float vSpeed = value / 2;

        return vSpeed;
    }
}
