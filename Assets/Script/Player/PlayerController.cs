﻿using System;
using System.Collections;
using System.Collections.Generic;
using GeneralClass;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private PlayerAttack playerAttack;
    private PlayerStats playerStats;
    private PlayerInteraction playerInteraction;
    private PlayerMovement playerMovement;
    private PlayerHealth playerHealth;

    public List<Upgrade> upgrades;

    public int money;

    public GameObject icon;

    public bool playerAsControl;
    public GameObject swordScaler;

    public bool playerBuff;
    
    
    void Start()
    {
        playerAttack = GetComponent<PlayerAttack>();
        playerStats = GetComponent<PlayerStats>();
        playerInteraction = GetComponent<PlayerInteraction>();
        playerMovement = GetComponent<PlayerMovement>();
        playerHealth = GetComponent<PlayerHealth>();
        icon.SetActive(true);
    }


    public void AddUpgrade(Upgrade upgrade)
    {
        upgrades.Add(upgrade);
        playerAttack.ApplyAttack();
        playerStats.ApplyStats();
    }

    public void Update()
    {
        swordScaler.SetActive(playerAsControl);
    }
}
