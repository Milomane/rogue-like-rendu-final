﻿using System;
using System.Collections;
using System.Collections.Generic;
using AudioAutoPlayerSpace;
using UnityEngine;
using UnityEngine.Rendering.Universal;
using UnityEngine.Audio;


public class PlayerMovement : MonoBehaviour
{
    public float speed;
    private Rigidbody2D _rigidbody2D;
    private Vector3 moveVelocity;
    private Vector3 knockbackVelocity;
    public float maxSpeed, acceleration;
    private Vector3 moveDirection;
    
    private bool dash;
    public float dashCooldown;
    private float dashTimer;
    public float dashDistance, dashSpeed;
    [SerializeField] private LayerMask dashLayerMask;
    private TrailRenderer effetDash;
    public GameObject dashEffect;

    public Vector3 additionalVector;
    public float décélération;

    public float knockbackLerpValue = 0.1f;

    private Animator anim;

    private InputManager _inputManager;
    private PlayerController playerController;
    
    public AudioClip dashClip;
    public AudioMixerGroup healthMixerGroup;

    private void Start()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        effetDash = GetComponent<TrailRenderer>();
        anim = GetComponent<Animator>();
        _inputManager = GameObject.FindObjectOfType<InputManager>();
        playerController = GetComponent<PlayerController>();
        
        dashEffect.GetComponent<ParticleSystem>().enableEmission = false;
    }

    private void Update()
    {
        if (playerController.playerAsControl)
        {
            if (Time.deltaTime > 0)
            {
                anim.SetFloat("speed", speed);
                float moveX = 0f;
                float moveY = 0f;

                //Déplacement Haut
                if (_inputManager.GetButton("Up") || Input.GetAxis("Vertical") > 0)
                {
                    moveY = +1f;
                    anim.SetFloat("yInput", 1);
                    anim.SetFloat("xInput", 0);

                    //Si diagonale Haut Droite
                    if (_inputManager.GetButton("Right"))
                    {
                        anim.SetFloat("yInput", 0); //empeche l'anim vers le haut
                        anim.SetFloat("xInput", 1); //anim vers la droite
                    }

                    //Si diagonale Haut Gauche
                    if (_inputManager.GetButton("Left"))
                    {
                        anim.SetFloat("yInput", 0); //empeche l'anim vers le haut
                        anim.SetFloat("xInput", -1); //anim vers la gauche
                    }
                }

                //Déplacement Bas
                if (_inputManager.GetButton("Down") || Input.GetAxis("Vertical") < 0)
                {
                    moveY = -1f;
                    anim.SetFloat("yInput", -1);
                    anim.SetFloat("xInput", 0);

                    //Si diagonale Bas Droite
                    if (_inputManager.GetButton("Right"))
                    {
                        anim.SetFloat("yInput", 0); //empeche l'anim vers le bas
                        anim.SetFloat("xInput", 1); //anim vers la droite
                    }

                    //Si diagonale Bas Gauche
                    if (_inputManager.GetButton("Left"))
                    {
                        anim.SetFloat("yInput", 0); //empeche l'anim vers le bas
                        anim.SetFloat("xInput", -1); //anim vers la gauche
                    }
                }

                //Déplacement Droite
                if (_inputManager.GetButton("Right") || Input.GetAxis("Horizontal") > 0)
                {
                    moveX = +1f;
                    anim.SetFloat("yInput", 0);
                    anim.SetFloat("xInput", 1);

                    //Si diagonale Droite Haut
                    if (_inputManager.GetButton("Up"))
                    {
                        anim.SetFloat("yInput", 0); //empeche l'anim vers le haut
                        anim.SetFloat("xInput", 1); //anim vers la droite
                    }

                    //Si diagonale Droite Bas
                    if (_inputManager.GetButton("Down"))
                    {
                        anim.SetFloat("yInput", 0); //empeche l'anim vers le bas
                        anim.SetFloat("xInput", 1); //anim vers la droite
                    }
                }

                //Déplacement Gauche
                if (_inputManager.GetButton("Left") || Input.GetAxis("Horizontal") < 0)
                {
                    moveX = -1f;
                    anim.SetFloat("yInput", 0);
                    anim.SetFloat("xInput", -1);

                    //Si diagonale Gauche Haut
                    if (_inputManager.GetButton("Up"))
                    {
                        anim.SetFloat("yInput", 0); //empeche l'anim vers le haut
                        anim.SetFloat("xInput", -1); //anim vers la gauche
                    }

                    //Si diagonale Gauche Bas
                    if (_inputManager.GetButton("Down"))
                    {
                        anim.SetFloat("yInput", 0); //empeche l'anim vers le bas
                        anim.SetFloat("xInput", -1); //anim vers la gauche
                    }
                }

                moveDirection = new Vector3(moveX, moveY).normalized;

                if (moveX != 0 || moveY != 0)
                {
                    speed += acceleration;

                    if (speed > maxSpeed)
                    {
                        speed = maxSpeed;
                    }

                    moveVelocity = moveDirection * speed + additionalVector;
                }
                else
                {
                    speed = 0;
                    moveVelocity = moveDirection * speed + additionalVector;
                }

                if (dashTimer <= 0)
                {
                    if (_inputManager.GetButtonDown("Dash") || Input.GetKeyDown(KeyCode.JoystickButton4))
                    {
                        dashTimer = dashCooldown;
                        dash = true;
                        anim.SetTrigger("Dash");
                        StartCoroutine(DashEffect());
                    }
                }
                else
                {
                    dashTimer -= Time.deltaTime;
                }
                

                additionalVector = Vector3.Lerp(additionalVector, Vector3.zero, décélération);
            }
        }
    }

    private void FixedUpdate()
    {
        if (playerController.playerAsControl)
        {
            _rigidbody2D.MovePosition(transform.position + (moveVelocity + knockbackVelocity) * Time.deltaTime);

            if (dash)
            {
                effetDash.enabled = effetDash.enabled;
                AudioAutoPlayer.Play(dashClip, transform.position, healthMixerGroup, 0.5f);
                Vector3 dashPosition = transform.position + moveDirection * dashSpeed * dashDistance;

                RaycastHit2D raycastHit2D = Physics2D.Raycast(transform.position, moveDirection, dashDistance, dashLayerMask);
                if (raycastHit2D.collider != null)
                    dashPosition = raycastHit2D.point;
            
                _rigidbody2D.MovePosition(dashPosition);
                dash = false;
            }
        }
    }
    
    IEnumerator DashEffect() //ajoute un effet pour le dash
    {
        dashEffect.GetComponent<ParticleSystem>().enableEmission = true;
      
        yield return new WaitForSeconds(0.1f);
        
        dashEffect.GetComponent<ParticleSystem>().enableEmission = false;
        
        yield return new WaitForSeconds(0.3f);
        anim.SetBool("Dash", false);
        
    }

    public void AddAdditionalVector(Vector3 commeJeVeux) //nom temporaire //propulsion en avant lors d'une attaque
    {
        additionalVector += commeJeVeux;
    }

    public IEnumerator Knockback(float power, Vector3 damageSourcePos)
    {
        knockbackVelocity = Vector3.Normalize(transform.position - damageSourcePos) * power;

        while (knockbackVelocity.magnitude > 0.01f)
        {
            knockbackVelocity = Vector3.Lerp(knockbackVelocity, Vector3.zero, knockbackLerpValue);
            yield return new WaitForSeconds(Time.deltaTime);
        }
        
        knockbackVelocity = Vector3.zero;
    }
}
