﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class PlayerCinematiqueMouvement : MonoBehaviour
{
    public Vector2 destination;
    public float speed = 200f;
    public float minDistanceToReach = .02f;
    
    public PlayerController playerController;
    public Animator animator;
    public Rigidbody2D rb;
    
    
    void Start()
    {
        playerController = GetComponent<PlayerController>();
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
    }

    public IEnumerator MovePlayer(Vector2 destination, bool playerReturnControl)
    {
        if (!playerController.playerAsControl)
        {
            while (Vector2.Distance(destination, (Vector2)transform.position) > minDistanceToReach)
            {
                Vector2 movementVector = destination - (Vector2)transform.position;
                movementVector = movementVector.normalized;
            
                animator.SetFloat("xInput", movementVector.x);
                animator.SetFloat("yInput", movementVector.y);
                animator.SetFloat("speed", 1);
            
                rb.AddForce(movementVector * speed * Time.deltaTime);
                
                yield return new WaitForSeconds(Time.deltaTime);
            }
            animator.SetFloat("speed", 0);
            rb.velocity = Vector2.zero;

            if (playerReturnControl)
            {
                playerController.playerAsControl = true;
            }
        }

        yield return null;
    }
}
