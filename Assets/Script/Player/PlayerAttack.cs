﻿using System;
using System.Collections;
using System.Collections.Generic;
using GeneralClass;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    public GameObject weapon;
    public SwordScript swordScript;
    private PlayerController playerController;
    
    public int attack;
    public int speed;
    public int range;
    public int knockback;
    public int angle;
    public int stunTime;

    private int baseAttack;
    private int baseSpeed;
    private int baseRange;
    private int baseKnockback;
    private int baseAngle;
    private int baseStunTime;

    public float minSpeed;

    void Start()
    {
        playerController = GetComponent<PlayerController>();
        
        if (weapon.GetComponent<SwordScript>())
        {
            swordScript = weapon.GetComponent<SwordScript>();
        }

        baseAttack = attack;
        baseSpeed = speed;
        baseRange = range;
        baseKnockback = knockback;
        baseAngle = angle;
        baseStunTime = stunTime;
    }
    void Update()
    {
        if (attack <= 0) attack = 1;
        if (speed <= 0) speed = 1;
        if (range <= 0) range = 1;
        if (knockback <= 0) knockback = 1;
        if (angle <= 0) angle = 1;
        if (stunTime <= 0) stunTime = 1;
        
        if (swordScript != null)
        {
            swordScript.attackDamage = AttackCalc(attack);
            swordScript.chargeAttackDamage = ChargeAttackCalc(attack);
            swordScript.attackCooldown = SpeedCalc(speed);
            swordScript.chargeCooldown = SpeedCalc(speed) * 2;
            swordScript.colliderDistance = RangeCalc(range);
            swordScript.colliderCount = ColliderCountCalc(angle);
            swordScript.knockback = KnockbackCalc(knockback);
            swordScript.attackAngle = AngleCalc(angle);
            swordScript.stunTime = StunTimeCalc(stunTime);
        }
    }

    public void ApplyAttack()
    {
        attack = baseAttack;
        speed = baseSpeed;
        range = baseRange;
        knockback = baseKnockback;
        angle = baseAngle;
        stunTime = baseStunTime;

        foreach (var upgrade in playerController.upgrades)
        {
            attack += upgrade.weaponUpgrade.attack;
            speed += upgrade.weaponUpgrade.speed;
            range += upgrade.weaponUpgrade.range;
            knockback += upgrade.weaponUpgrade.knockback;
            angle += upgrade.weaponUpgrade.angle;
            stunTime += upgrade.weaponUpgrade.stuntime;
        }
    }

    int AttackCalc(int value)
    {
        int vattack = value;

        return vattack;
    }
    int ChargeAttackCalc(int value)
    {
        int vchargeAttack = value + value/2 + 1;

        return vchargeAttack;
    }
    
    float SpeedCalc(int value)
    {
        float vspeed = minSpeed / value;

        return vspeed;
    }
    
    float RangeCalc(int value)
    {
        float vrange = (float) value / 5;

        return vrange;
    }

    int ColliderCountCalc(int value)
    {
        int vcolliderCount = value / 6 + 2;

        return vcolliderCount;
    }

    float KnockbackCalc(int value)
    {
        float vknockback = value / 2;

        return vknockback;
    }
    
    float AngleCalc(int value)
    {
        float vangle = value * 10f;

        return vangle;
    }
    
    float StunTimeCalc(int value)
    {
        float vstunTime = (float)value / 2;

        return vstunTime;
    }
}
