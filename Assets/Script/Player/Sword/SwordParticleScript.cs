﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordParticleScript : MonoBehaviour
{
    public Transform scaleCopyObject;
    public ParticleSystem particleSystem;
    
    void Start()
    {
        particleSystem = GetComponent<ParticleSystem>();
    }
    
    void Update()
    {
        var sh = particleSystem.shape;
        sh.scale = new Vector3(scaleCopyObject.localScale.x, sh.scale.y, sh.scale.z);
        sh.position = new Vector3(scaleCopyObject.localScale.x, sh.position.y, sh.position.z);
    }
}
