﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO.Compression;
using AudioAutoPlayerSpace;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SocialPlatforms;
using Random = UnityEngine.Random;


public class SwordScript : MonoBehaviour
{
    public float rotateSpeed;
    
    public int colliderCount;
    
    public int attackDamage;
    public int chargeAttackDamage;
    
    public float stunTime;
    public float knockback;
    public float attackAngle;
    public float colliderRadius;
    public float colliderDistance;
    public float attackCooldown;
    public float chargeCooldown;

    public float comboStopTime = 1f;

    public Color colorChargeSword;

    public float pullForwardForce;

    private float attackTimer;

    public GameObject animSwordObject;
    public GameObject scaleSwordObject;
    public GameObject cooldownObject;
    public Transform cooldownBar;
    public SpriteRenderer swordSprite;
    private Animator animator;

    private PlayerMovement playerMovement;
    private PlayerController playerController;
    public Animator playerAnimator;

    public GameObject chargeSlashObject;
    public GameObject estocSlashObject;
    public GameObject slashObject;

    private float startTime;
    private bool startClickingBefore;
    private float lastAttackInput;
    private float lastDashTime;

    public GameObject prefabGizmoCircle;
    
    public enum AttackType {slash = 1, chargeSlash = 2, jumpSlash = 3, precharge = 4};

    private bool attackRight;

    [Header("Audio")] 
    public AudioClip slash;
    public AudioClip hit;
    public AudioMixerGroup mixerEffect;

    private InputManager _inputManager;
    

    void Start()
    {
        lastDashTime = 10;
        
        playerMovement = FindObjectOfType<PlayerMovement>();
        playerController = FindObjectOfType<PlayerController>();
        
        animator = GetComponent<Animator>();
        _inputManager = GameObject.FindObjectOfType<InputManager>();
    }
    
    
    void Update()
    {
        if (Time.deltaTime > 0)
        {
            if (playerController.playerAsControl)
            {
                if (_inputManager.GetButtonDown("Dash") && (_inputManager.GetButton("Up") || _inputManager.GetButton("Down") || _inputManager.GetButton("Left") || _inputManager.GetButton("Right"))|| Input.GetKeyDown(KeyCode.JoystickButton4)) DashInput();

                // Sword rotation
                Vector3 mouseWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            
                Vector3 vectorToTarget = Vector3.zero;

                if (Input.GetAxis("Mouse X") != 0 || Input.GetAxis("Mouse Y") != 0)
                {
                    vectorToTarget = new Vector3(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"), 0);
                }
                else
                {
                    vectorToTarget = mouseWorldPos - transform.position;
                }
                
                float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;

                if (angle < 0) swordSprite.sortingOrder = 1;
                else swordSprite.sortingOrder = -1;
                
                Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
                transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * rotateSpeed);
                scaleSwordObject.transform.localScale = new Vector3(Mathf.Pow(colliderDistance/1.8f, 1.2f), 1, 1);
                
                // Attack
                if (lastAttackInput > comboStopTime)
                {
                    attackRight = false;
                    ResetAnimSword();
                }

                if (attackTimer <= 0)
                {
                    if (_inputManager.GetButtonDown("Attack") || Input.GetKeyDown(KeyCode.JoystickButton5))
                    {
                        startTime = 0;
                        StartCoroutine(AnimAttack(AttackType.precharge, attackAngle));
                    }

                    if (!startClickingBefore)
                    {
                        if (_inputManager.GetButton("Attack") || Input.GetKey(KeyCode.JoystickButton5))
                        {
                            lastAttackInput = 0;
                            startTime += Time.deltaTime;
                            
                            if (startTime >= chargeCooldown)
                            {
                                swordSprite.color = Color.white;
                                animator.SetBool("blinking", true);
                            }
                        }
                        if (_inputManager.GetButtonUp("Attack") || Input.GetKeyUp(KeyCode.JoystickButton5))
                        {
                            if (startTime >= chargeCooldown) // Charge attack
                            {
                                // ANIM CHARGE ATTACK
                                StartCoroutine(AnimAttack(AttackType.chargeSlash, attackAngle));
                                
                                // Charge attack damage
                                StartCoroutine(Attack(chargeAttackDamage, pullForwardForce * 2, attackAngle, colliderCount, colliderDistance, colliderRadius, chargeSlashObject));
                            }
                            else
                            {
                                if (lastDashTime < comboStopTime) // Dash attack
                                {
                                    lastDashTime = 10;
                                    // ANIM DASH ATTACK
                                    ResetAnimSword();
                                    StartCoroutine(AnimAttack(AttackType.jumpSlash, 30));

                                    // Attack damage
                                    StartCoroutine(Attack(attackDamage * 2, pullForwardForce * 0.5f, 0, colliderCount,
                                        colliderDistance * 1.7f, colliderRadius, estocSlashObject));
                                }
                                else // Normal attack
                                {
                                    // ANIM ATTAQUE
                                    ResetAnimSword();
                                    StartCoroutine(AnimAttack(AttackType.slash, attackAngle));

                                    // Attack damage
                                    StartCoroutine(Attack(attackDamage, pullForwardForce, attackAngle, colliderCount,
                                        colliderDistance, colliderRadius, slashObject));
                                }
                            }

                            attackTimer = attackCooldown;
                            StartCoroutine(CooldownAnim(attackCooldown));
                        }
                    }
                }
                else
                {
                    if (_inputManager.GetButtonDown("Attack") || Input.GetKeyDown(KeyCode.JoystickButton5))
                    {
                        startClickingBefore = true;
                    }
                    attackTimer -= Time.deltaTime;
                }
                
                if (!_inputManager.GetButton("Attack") || Input.GetKey(KeyCode.JoystickButton5))
                {
                    startClickingBefore = false;
                }
            
                lastAttackInput += Time.deltaTime;
                lastDashTime += Time.deltaTime;
            }
        }
    }

    public IEnumerator Attack(int damage, float pullForce, float _attackAngle, int _colliderCount, float _colliderDistance, float _colliderRadius, GameObject slashO)
    {
        List<GameObject> ennemyHit = new List<GameObject>();
        
        Vector3 mouseWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector3 vectorToTarget = Vector3.zero;
        
        if (Input.GetAxis("Mouse X") != 0 || Input.GetAxis("Mouse Y") != 0)
        {
            vectorToTarget = new Vector3(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"), 0);
        }
        else
        {
            vectorToTarget = mouseWorldPos - transform.position;
        }
        
        
        for (int i = 0; i < colliderCount; i++)
        {
            // This system create one or more sphere cast depending on multiple parameter to deal damage to ennemies
            float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
            angle = angle - ((_attackAngle * 2) / (_colliderCount - 1) * i) + (_attackAngle * 2) / (_colliderCount - 1) * (_colliderCount-1)/2;
            angle = angle * Mathf.Deg2Rad;
            
            Vector3 pos = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle)) * _colliderDistance + new Vector2(transform.position.x, transform.position.y);
            
            // Add ennemi to hit list
            RaycastHit2D[] hits;
            hits = Physics2D.CircleCastAll(pos, _colliderRadius *  Mathf.Pow(colliderDistance * 1.4f, 1.1f), Vector3.forward);
            

            foreach (var hit in hits)
            {
                if (!ennemyHit.Contains(hit.collider.gameObject))
                {
                    ennemyHit.Add(hit.collider.gameObject);
                }
            }
            
            // Draw gizmo circle
            if (FindObjectOfType<GameplayManager>().debugMode)
            {
                GameObject c = Instantiate(prefabGizmoCircle, pos, Quaternion.identity);
                c.transform.localScale = new Vector3(_colliderRadius*2 * Mathf.Pow(colliderDistance * 1.4f, 1.1f), _colliderRadius*2 * Mathf.Pow(colliderDistance * 1.4f, 1.1f), 1);
            }
            
            // Slash sprite
            if (i == Mathf.RoundToInt(_colliderCount / 2))
            {
                StartCoroutine(Slash(_colliderDistance, slashO));
            }
        }
        
        // Play sound
        AudioAutoPlayer.Play(slash, transform.position, mixerEffect,1, Random.Range(0.8f, 1.2f));
        
        // Pull the player forward
        Vector3 pullVector = vectorToTarget.normalized * pullForce;
        playerMovement.AddAdditionalVector(pullVector);
            
        // Shake Camera
        Camera.main.GetComponent<CameraController>().Shake(0.4f, 0.2f, 5, 0);
        
        // Hit all ennemies
        foreach (var ennemy in ennemyHit)
        {
            // Attaque ennemi
            if (ennemy.GetComponent<ennemyController>())
            {
                AudioAutoPlayer.Play(hit, transform.position, mixerEffect,0.5f);
                ennemy.GetComponent<ennemyController>().TakeDamage(damage, new Vector3(transform.position.x, transform.position.y ,transform.position.z), stunTime, knockback);
            }

            if (ennemy.GetComponent<Boss>())
            {
                AudioAutoPlayer.Play(hit, transform.position, mixerEffect,0.5f);
                StartCoroutine(ennemy.GetComponent<Boss>().TakeDamage(damage));
            }
            
            // Destruct object
            if (ennemy.GetComponent<DestructibleObject>())
            {
                StartCoroutine(ennemy.GetComponent<DestructibleObject>().BreakObject());
            }
        }

        yield return null;
    }

    public IEnumerator Slash(float range, GameObject slashObj)
    {
        slashObj.GetComponent<Animator>().SetBool("Slash", true);
        slashObj.transform.position = transform.position;
        slashObj.transform.rotation = transform.rotation;
        float scale = attackRight ? -1f : 1f; 
        slashObj.transform.localScale = new Vector3(range*1.5f,range*1.5f*scale,1);
        
        //slashObject.transform.localScale = new Vector3(_colliderDistance, _colliderDistance * (_colliderCount /2) / 1.2f, 1);
        yield return new WaitForSeconds(0.1f);
        
        slashObj.GetComponent<Animator>().SetBool("Slash", false);
    }

    public IEnumerator AnimAttack(AttackType attack, float angle)
    {
        switch (attack)
        {
            case AttackType.slash :
                if (!attackRight) 
                    animSwordObject.transform.DOLocalRotate(new Vector3(0,0, -angle), attackCooldown/3, RotateMode.Fast);
                else 
                    animSwordObject.transform.DOLocalRotate(new Vector3(0,0, angle), attackCooldown/3, RotateMode.Fast);
                attackRight = !attackRight;
                
                playerAnimator.SetBool("Attack", true);
                yield return new WaitForSeconds(Time.deltaTime);
                playerAnimator.SetBool("Attack", false);
                break;
            
            case AttackType.chargeSlash :
                if (startTime >= chargeCooldown)
                {
                    if (attackRight)
                    {
                        animSwordObject.transform.DOLocalRotate(new Vector3(0, 0, angle), attackCooldown/3);
                        attackRight = !attackRight;
                    }
                    else
                    {
                        animSwordObject.transform.DOLocalRotate(new Vector3(0, 0, -angle), attackCooldown/3);
                        attackRight = !attackRight;
                    }
                }
                animator.SetBool("blinking", false);
                
                playerAnimator.SetBool("Attack chargé", true);
                yield return new WaitForSeconds(Time.deltaTime);
                playerAnimator.SetBool("Attack chargé", false);
                break;
            case AttackType.precharge :
                swordSprite.DOColor(colorChargeSword, chargeCooldown);
                if (attackRight)
                {
                    animSwordObject.transform.DOLocalRotate(new Vector3(0, 0, -angle - 15), chargeCooldown);
                }
                else
                {
                    animSwordObject.transform.DOLocalRotate(new Vector3(0, 0, angle + 15), chargeCooldown);
                }
                break;
            
            case AttackType.jumpSlash :
                
                animSwordObject.transform.DOLocalMoveX(1, attackCooldown/2);
                yield return new WaitForSeconds(attackCooldown/2);
                animSwordObject.transform.DOLocalMoveX(0, attackCooldown/2);
                
                playerAnimator.SetBool("Attack", true);
                yield return new WaitForSeconds(Time.deltaTime);
                playerAnimator.SetBool("Attack", false);
                break;
        }
        yield return null;
    }

    public IEnumerator CooldownAnim(float time)
    {
        cooldownObject.SetActive(true);
        cooldownBar.localScale = new Vector3(0, 1, 1);

        while (attackTimer > 0)
        {
            cooldownBar.localScale = new Vector3(1 - attackTimer/time, 1, 1);
            yield return new WaitForSeconds(Time.deltaTime);
        }

        cooldownObject.SetActive(false);
    }

    public void ResetAnimSword()
    {
        DOTween.Kill(animSwordObject.transform);
        DOTween.Kill(swordSprite);
        swordSprite.color = Color.white;
        
        animSwordObject.transform.DOLocalRotate(new Vector3(0, 0, 0), 0.1f);
        animSwordObject.transform.DOLocalMoveX(0, 0.1f);
    }

    public void DashInput()
    {
        lastDashTime = 0;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;

        for (int i = 0; i < colliderCount; i++)
        {
            Vector3 mouseWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            
            Vector3 vectorToTarget = Vector3.zero;

            if (Input.GetAxis("Mouse X") != 0 || Input.GetAxis("Mouse Y") != 0)
            {
                vectorToTarget = new Vector3(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"), 0);
            }
            else
            {
                vectorToTarget = mouseWorldPos - transform.position;
            }
            
            float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
            angle = angle - ((attackAngle * 2) / (colliderCount - 1) * i) + (attackAngle * 2) / (colliderCount - 1) * (colliderCount-1)/2;

            angle = angle * Mathf.Deg2Rad;

            Vector3 pos = new Vector3(Mathf.Cos(angle), Mathf.Sin(angle), 0) * colliderDistance + transform.position;
            
            Gizmos.DrawWireSphere(pos,  colliderRadius *  Mathf.Pow(colliderDistance * 1.4f, 1.1f));
        }
    }
}
