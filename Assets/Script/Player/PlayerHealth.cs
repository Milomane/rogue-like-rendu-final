using System;
using System.Collections;
using System.Collections.Generic;
using AudioAutoPlayerSpace;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.Audio;

public class PlayerHealth : MonoBehaviour
{
    public int health;
    public int numOfHeart;
    public int maxMaxHealth = 32;
    public Image[] hearts;

    public float radiusEnnemyKnockback = .25f;
    public float maxEnnemyKnockbackForce = 8;

    private SpriteRenderer player; //récupère le sprite du player
    public Image redScreenEffect;
    public GameObject gameOver,lastheart;

    private Animator anim;
    private Animator shieldAnim;
    public Animator heartAnim;
    private PlayerMovement playerMovement;
    
    private int lastHeartCount;

    public AudioClip hurtEffect;
    public AudioClip gameOverClip;
    public AudioClip deathClip;
    public AudioMixerGroup healthMixerGroup;

  //  private bool takeDamage;
    public float damageCooldown;
    public float cooldwon = 1f;
    
    void Start()
    {
        lastHeartCount = numOfHeart;

        damageCooldown = cooldwon;

        hearts = FindObjectOfType<HeartArray>().hearts;
        
        anim = GetComponent<Animator>();
        player = GetComponent<SpriteRenderer>();
        playerMovement = GetComponent<PlayerMovement>();
        gameOver = FindObjectOfType<HeartArray>().gameOverScreen;
        gameOver.SetActive(false);
        lastheart = FindObjectOfType<HeartArray>().uniqueHeart;
        heartAnim = lastheart.GetComponent<Animator>();

        redScreenEffect = GameObject.Find("RedScreenEffect").GetComponent<Image>();
        redScreenEffect.color = new Color(redScreenEffect.color.r, redScreenEffect.color.g, redScreenEffect.color.b, 0f); //rends invisible l'écran rouge

      //  takeDamage = false;
    }
    
    void Update()
    {
        if (numOfHeart > maxMaxHealth) // si le joueur recup plus de PV que le maximum que peut supporter le jeu
            numOfHeart = maxMaxHealth;
        
        if (health > numOfHeart) //si le joueur recup plus de vie que la max autorisé
            health = numOfHeart;
        
        for (int i = 0; i < hearts.Length; i++)
        {
            shieldAnim = hearts[i].GetComponent<Animator>();
            
            if (i < health) //quand i est plus petit que le nombre de vie
                shieldAnim.SetBool("Hurt", false); //le joueur a une vie
            else
                shieldAnim.SetBool("Hurt", true);  //le joueur a une vie en moins
            
            if (i < numOfHeart) //pour afficher le nombre max de vie
                hearts[i].enabled = true;
            else
            {
                hearts[i].enabled = false;
            }
        }
        
        if (health < 1) 
            heartAnim.SetBool("critique", true); 
        else 
            heartAnim.SetBool("critique", false); 

        if (health < 0)
        {
            if (!gameOver.activeSelf)
            {
                anim.SetBool("Death", true);
                StartCoroutine(PlayerDeath());
            }
        }

        // Quand la vie du joueur a augmenté, on donne la vie qui correspond a l'augmentation
        if (lastHeartCount < numOfHeart)
            health += numOfHeart - lastHeartCount;
        if (lastHeartCount > numOfHeart)
            health += numOfHeart - lastHeartCount;
        lastHeartCount = numOfHeart;

        if (damageCooldown > 0)
            damageCooldown -= Time.deltaTime;
    }

    IEnumerator PlayerDeath()
    {
        GetComponent<PlayerMovement>().enabled = false;
        heartAnim.SetBool("Death", true);
        yield return new WaitForSeconds(1.1f);
        gameOver.SetActive(true);
        AudioAutoPlayer.Play(gameOverClip, transform.position, healthMixerGroup, .7f);
        AudioAutoPlayer.Play(deathClip, transform.position, healthMixerGroup, .5f);
        Time.timeScale = 0;
    }
    
    public void RecupHealth(int heal) //fonction a appelé quand le player recup de la vie
    {
        health += heal;
    }

    public void TakeDamage(int damage, float knockbackPower, Vector3 damageSourcePos) //fonction a appelé quand le player preds de la vie
    {
        if (damageCooldown < 0)
        {
            AudioAutoPlayer.Play(hurtEffect, transform.position, healthMixerGroup);
            
            //takeDamage = true;
            damageCooldown = cooldwon;
            
            health -= damage;
            StartCoroutine(EffectDamage());  //effet visuel à l'écran
            anim.SetTrigger("Hurt");

            StartCoroutine(playerMovement.Knockback(knockbackPower, damageSourcePos));

            RaycastHit2D[] hits;
            hits = Physics2D.CircleCastAll(transform.position, radiusEnnemyKnockback, Vector2.up, .5f);

            foreach (var hit in hits)
            {
                if (hit.collider.GetComponent<ennemyController>())
                {
                    float power = (1 - radiusEnnemyKnockback / Vector3.Distance(transform.position, hit.collider.transform.position)) * maxEnnemyKnockbackForce;
                
                    hit.collider.GetComponent<ennemyController>().Knockback(transform.position, power);
                }
            }
        
            if (health > 0)
                StartCoroutine(FlashingPlayer()); //effet visuel de player
        }
    }

    IEnumerator EffectDamage()
    {
        //fait apparaitre une redscreen puis disparait en fade out
        redScreenEffect.color = new Color(redScreenEffect.color.r, redScreenEffect.color.g, redScreenEffect.color.b, 1f);
        redScreenEffect.DOFade(0f, 0.5f);
        
        Camera.main.GetComponent<CameraController>().Shake(0.4f, 0.2f, 5, 45); //provoque un sake de la caméra
        
        yield return null;
    }

    IEnumerator FlashingPlayer() //rends le player visible/invisble plusieurs fois à la suite
    {
        for(var n = 0; n < 4; n++)
        {
            player.enabled = true;
            yield return new WaitForSeconds(0.1f);
            player.enabled = false; 
            yield return new WaitForSeconds(0.1f);
        }
        player.enabled = true;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawWireSphere(transform.position, radiusEnnemyKnockback);
    }
}
