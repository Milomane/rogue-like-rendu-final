﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteraction : MonoBehaviour
{
    public float distanceToPlayer = 1;
    public float interactionSphereRadius = 1;

    public LayerMask layer;
    
    private InputManager _inputManager;
    private PlayerController playerController;

    private void Start()
    {
        _inputManager = GameObject.FindObjectOfType<InputManager>();
        playerController = GetComponent<PlayerController>();
    }

    public void Update()
    {
        if (playerController.playerAsControl)
        {
            Vector3 mouseWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector3 vectorToTarget = new Vector3(mouseWorldPos.x, mouseWorldPos.y, 0) - transform.position;
            
            bool touch = false;

            RaycastHit2D[] hits;
            RaycastHit2D hit = new RaycastHit2D();
            hits = Physics2D.CircleCastAll(transform.position + vectorToTarget.normalized * distanceToPlayer,
                interactionSphereRadius, Vector2.up, distanceToPlayer, layer);

            float distanceMax = 100;
            foreach (var h in hits)
            {
                if (Vector2.Distance(transform.position + vectorToTarget.normalized * distanceToPlayer, h.collider.transform.position) < distanceMax)
                {
                    distanceMax = Vector2.Distance(transform.position + vectorToTarget.normalized * distanceToPlayer, h.collider.transform.position);
                    hit = h;
                }
            }
            
            if (hits.Length > 0)
            {
                touch = true;
                
                if (hit.collider.tag == "UpgradeItem")
                {
                    if (hit.collider.GetComponent<UpgradeItem>())
                    {
                        hit.collider.GetComponent<UpgradeItem>().Detect();
                    
                        if (_inputManager.GetButtonDown("Interact") || Input.GetKeyDown(KeyCode.JoystickButton0))
                        {
                            hit.collider.GetComponent<UpgradeItem>().Interact();
                        }
                    }
                    else
                    {
                        hit.collider.GetComponent<RandomUpgradeItem>().Detect();
                    
                        if (_inputManager.GetButtonDown("Interact") || Input.GetKeyDown(KeyCode.JoystickButton0))
                        {
                            hit.collider.GetComponent<RandomUpgradeItem>().Interact();
                        }
                    }
                    
                }

                if (hit.collider.tag == "PNJ")
                {
                    hit.collider.GetComponent<PnjDialogue>().Detect();
                    
                    if (_inputManager.GetButtonDown("Interact") || Input.GetKeyDown(KeyCode.JoystickButton0))
                    {
                        hit.collider.GetComponent<PnjDialogue>().Interact();
                    }
                }
                
                if (hit.collider.tag == "PurchasableItem")
                {
                    hit.collider.GetComponent<PurchasableObject>().Detect();
                    
                    if (_inputManager.GetButtonDown("Interact") || Input.GetKeyDown(KeyCode.JoystickButton0))
                    {
                        hit.collider.GetComponent<PurchasableObject>().Interact();
                    }
                }
            }
        }
    }

    private void OnDrawGizmos()
    {
        Vector3 mouseWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector3 vectorToTarget = mouseWorldPos - transform.position;
        
        Gizmos.DrawWireSphere(transform.position + vectorToTarget.normalized * distanceToPlayer, interactionSphereRadius);
    }
}
