﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class GizmoCircle : MonoBehaviour
{
    public float timeBeforeDestroy = 1;
    
    void Start()
    {
        Destroy(gameObject, timeBeforeDestroy);
    }

    void Update()
    {
        transform.DOScale(Vector3.zero, timeBeforeDestroy);
    }
}
