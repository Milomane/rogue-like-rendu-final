﻿using System;
using System.Collections;
using System.Collections.Generic;
using AudioAutoPlayerSpace;
using UnityEngine;
using UnityEngine.Audio;
using Random = UnityEngine.Random;

public class DestructibleObject : MonoBehaviour
{
    private ParticleSystem particleSystem;
    private SpriteRenderer spriteRenderer;
    private ParticleSystemRenderer particleSystemRenderer;

    public bool destroyed;

    public ScriptableDestructibleObject destO;

    public AudioMixerGroup effectMixerGroup;

    void Start()
    {
        particleSystem = GetComponent<ParticleSystem>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        particleSystemRenderer = GetComponent<ParticleSystemRenderer>();

        spriteRenderer.sprite = destO.sprite;
        particleSystemRenderer.material = destO.particleMaterial;
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player" || other.tag == "Enemy")
        {
            StartCoroutine(BreakObject());
        }
    }

    public IEnumerator BreakObject()
    {
        if (!destroyed)
        {
            destroyed = true;
            particleSystem.Play();
            AudioAutoPlayer.Play(destO.breakSound, transform.position, effectMixerGroup, 1, Random.Range(.9f, 1.1f));
            spriteRenderer.sprite = destO.brokenSprite;
            
            Debug.Log(destO.lootProb);
            
            if (Random.Range(0f, 100f) < destO.lootProb)
            {
                int count = Random.Range(destO.minLoot, destO.maxLoot);
                for (int i = 0; i < count; i++)
                {
                    Instantiate(destO.loot, transform.position, Quaternion.identity);
                }
            }
            
            yield return null;
        }
    }
}
