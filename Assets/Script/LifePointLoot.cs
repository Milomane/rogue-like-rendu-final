﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AudioAutoPlayerSpace;
using UnityEngine.Audio;
using Random = UnityEngine.Random;

public class LifePointLoot : MonoBehaviour
{
    public int amount;
    private Rigidbody2D rb;
    
    public float maxPushForce;
    public float maxRotationForce;

    public AudioClip pickupLifeClip;
    public AudioMixerGroup mixer;
    
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<PlayerMovement>())
        {
            Debug.Log("colli player");
            other.gameObject.GetComponent<PlayerHealth>().RecupHealth(1);
            AudioAutoPlayer.Play(pickupLifeClip, transform.position, mixer, .75f);
            Destroy(gameObject);
        }
    }
}
