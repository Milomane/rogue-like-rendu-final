﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopDynamiqueLayer : MonoBehaviour
{
    private SpriteRenderer spriteRenderer;
    public SpriteRenderer shopKeeper;
    
    public GameObject[] allObjectToSort;
    private SpriteRenderer[] allObjectRendererToSort;
    
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        
        allObjectRendererToSort = new SpriteRenderer[allObjectToSort.Length];

        for (int i = 0; i < allObjectToSort.Length; i++)
        {
            allObjectRendererToSort[i] = allObjectToSort[i].GetComponent<SpriteRenderer>();
        }
    }
    void Update()
    {
        if (transform.position.y > FindObjectOfType<PlayerController>().transform.position.y)
        {
            spriteRenderer.sortingOrder = -21;
            for (int i = 0; i < allObjectToSort.Length; i++)
            {
                if (allObjectToSort[i].gameObject != null) allObjectRendererToSort[i].sortingOrder = -19;
            }

            shopKeeper.sortingOrder = -20;
        }
        else
        {
            spriteRenderer.sortingOrder = 18;
            for (int i = 0; i < allObjectToSort.Length; i++)
            {
                if (allObjectToSort[i].gameObject != null) allObjectRendererToSort[i].sortingOrder = 20;
            }
            
            shopKeeper.sortingOrder = 19;
        }
    }
}
