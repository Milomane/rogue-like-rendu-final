﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class NewItemPanelScript : MonoBehaviour
{
    public List<Upgrade> upgrade = new List<Upgrade>();

    public Image spriteBox;
    public TextMeshProUGUI textMeshPro;

    private Animator animator;

    private bool playingAnimation;

    void Start()
    {
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        if (upgrade.Count > 0)
        {
            if (!playingAnimation)
            {
                StartCoroutine(animationControl());
            }
            spriteBox.sprite = upgrade[0].sprite;
            textMeshPro.text = upgrade[0].itemName;
        }
    }

    public void TriggerPanel(Upgrade newUpgrade)
    {
        upgrade.Add(newUpgrade);
    }

    public IEnumerator animationControl()
    {
        animator.SetTrigger("TriggerPanel");
        playingAnimation = true;
        yield return new WaitForSeconds(0.5f);
        yield return new WaitForSeconds(animator.GetCurrentAnimatorStateInfo(0).length);
        Debug.Log("AnimationEnd");
        upgrade.RemoveAt(0);
        playingAnimation = false;
    }
}
