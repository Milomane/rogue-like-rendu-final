﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class InputDialogBox : MonoBehaviour
{
    private InputManager _inputManager;
    public GameObject keyItemPrefab, keyList;

    private string buttonToRebind = null;

    private Dictionary<string, TMP_Text> buttonToLabel;

    void Start()
    {
        _inputManager = GameObject.FindObjectOfType<InputManager>();
        
        //Créer un "KeyList Item" par boutton dans l'input Manager
        string[] buttonNames = _inputManager.GetButtonNames();
        buttonToLabel = new Dictionary<string, TMP_Text>();

       // foreach (string bn in buttonNames)
       for (int i = 0; i < buttonNames.Length; i++)
       {
           string bn;
           bn = buttonNames[i]; 
           
            GameObject go = (GameObject)Instantiate(keyItemPrefab);
            go.name = bn;
            go.transform.SetParent(keyList.transform, false);

            TMP_Text buttonNameText = go.transform.Find("Input Name").GetComponent<TMP_Text>();
            buttonNameText.text = _inputManager.inputName[i];
            
            TMP_Text keyNameText = go.transform.Find("Button/Key Name").GetComponent<TMP_Text>();
            keyNameText.text = _inputManager.GetKeyNameForButton(bn);
            buttonToLabel[bn] = keyNameText;

            UnityEngine.UI.Button keyBindButton = go.transform.Find("Button").GetComponent<UnityEngine.UI.Button>();
           keyBindButton.onClick.AddListener( () => { StartRebindFor(bn); } );
        }
    }

    private void Update()
    {
        if (buttonToRebind != null)
        {
            if (Input.anyKeyDown)
            {
                foreach (KeyCode kc in Enum.GetValues(typeof(KeyCode)))    
                {
                    Debug.Log(kc);
                    
                    if (Input.GetKeyDown(kc))
                    {
                        _inputManager.SetButtonForKey(buttonToRebind, kc);
                        buttonToLabel[buttonToRebind].text = kc.ToString();
                        buttonToRebind = null;
                        break;
                    }
                }
            }
        }
    }

    void StartRebindFor(string buttonName)
    {
        Debug.Log("StartRebindFor: " + buttonName);

        buttonToRebind = buttonName;
    }
}
