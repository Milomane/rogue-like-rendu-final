﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using TMPro;

public class InputManager : MonoBehaviour
{
    private Dictionary<string, KeyCode> buttonKeys;
    private Dictionary<string, KeyCode> controllerKeys;
    
    public static bool create;
    
    public string[] inputName;

    private void OnEnable() //se fait avant le start
    {
        buttonKeys = new Dictionary<string, KeyCode>();
        
        buttonKeys["Up"] = KeyCode.Z;
        buttonKeys["Down"] = KeyCode.S;
        buttonKeys["Left"] = KeyCode.Q;
        buttonKeys["Right"] = KeyCode.D;
        buttonKeys["Dash"] = KeyCode.Space;
        buttonKeys["Attack"] = KeyCode.Mouse0;
        buttonKeys["Interact"] = KeyCode.E;
        buttonKeys["Pause"] = KeyCode.Escape;
    }

    void Start()
    {
        if (create)
            Destroy(gameObject);
        
        else
        {
            create = true;
            DontDestroyOnLoad(this);
        }
    }

    public bool GetButton(string buttonName)
    {
        
        //TODO: vérifier si le jeu est supposé être en pause
        
        if (buttonKeys.ContainsKey(buttonName) == false)
        {
            Debug.LogError("InputManager : GetButtonDown -- No button named: " + buttonName);
            return false;
        }

        return Input.GetKey(buttonKeys[buttonName]);
    }
    
    public bool GetButtonDown(string buttonName)
    {
        
        //TODO: vérifier si le jeu est supposé être en pause
        
        if (buttonKeys.ContainsKey(buttonName) == false)
        {
            Debug.LogError("InputManager : GetButtonDown -- No button named: " + buttonName);
            return false;
        }

        return Input.GetKeyDown(buttonKeys[buttonName]);
    }
    
    public bool GetButtonUp(string buttonName)
    {
        
        //TODO: vérifier si le jeu est supposé être en pause
        
        if (buttonKeys.ContainsKey(buttonName) == false)
        {
            Debug.LogError("InputManager : GetButtonDown -- No button named: " + buttonName);
            return false;
        }
        return Input.GetKeyUp(buttonKeys[buttonName]);
    }

    public string[] GetButtonNames()
    {
        return buttonKeys.Keys.ToArray();
    }

    public string GetKeyNameForButton(string buttonName)
    {
        Debug.Log("Bonjour");
        Debug.Log(buttonName);

        if (buttonKeys.ContainsKey(buttonName) == false)
        {
            Debug.LogError("InputManager : GetButtonDown -- No button named: " + buttonName);
            return "N/A";
        }

        return buttonKeys[buttonName].ToString();
    }

    public void SetButtonForKey(string buttonName, KeyCode keyCode)
    {
        buttonKeys[buttonName] = keyCode;
    }

    private Dictionary<string, TMP_Text> buttonToLabel;
    private GameObject go;

    public void Reset()
    {
        buttonKeys["Up"] = KeyCode.Z;
        buttonKeys["Down"] = KeyCode.S;
        buttonKeys["Left"] = KeyCode.Q;
        buttonKeys["Right"] = KeyCode.D;
        buttonKeys["Dash"] = KeyCode.Space;
        buttonKeys["Attack"] = KeyCode.Mouse0;
        buttonKeys["Attack Charge"] = KeyCode.Mouse1;
        buttonKeys["Interact"] = KeyCode.E;
        buttonKeys["Pause"] = KeyCode.Escape;
        
        string[] buttonNames = GetButtonNames();
        buttonToLabel = new Dictionary<string, TMP_Text>();

        for (int i = 0; i < buttonNames.Length; i++)
        {
            string bn;
            bn = buttonNames[i];
            
            go = GameObject.Find(bn);

            TMP_Text keyNameText = go.transform.Find("Button/Key Name").GetComponent<TMP_Text>();
            keyNameText.text = GetKeyNameForButton(bn);
            buttonToLabel[bn] = keyNameText;
        }
    }
}