﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartUI : MonoBehaviour
{
    public float randomRange = 10;
    
    void Start()
    {
        transform.rotation = Quaternion.Euler(new Vector3(0,0, Random.Range(-randomRange, randomRange)));
    }
}
