﻿using System;
using System.Collections;
using System.Collections.Generic;
using GeneralClass;
using TMPro;
using UnityEngine;

public class UpgradeItem : MonoBehaviour
{
    public Upgrade upgrade;
    public SpriteRenderer spriteRenderer;
    public float outlineThickness = .4f;
    public bool showInfo;

    public GameObject infoUI;
    public TextMeshProUGUI textInfo;
    public TextMeshProUGUI textInfoSizeFitter;

    private float timeBeforeClose;
    
    void Start()
    {
        spriteRenderer.sprite = upgrade.sprite;
        
        // Set text info
        string info = "";

        if (upgrade.weaponUpgrade.angle != 0 || upgrade.weaponUpgrade.attack != 0 ||
            upgrade.weaponUpgrade.knockback != 0 || upgrade.weaponUpgrade.speed != 0 ||
            upgrade.weaponUpgrade.range != 0 || upgrade.weaponUpgrade.stuntime != 0)
        {
            info += "Arme :\n";

            info += AddStatInfoText(upgrade.weaponUpgrade.attack, "<sprite=0>");
            info += AddStatInfoText(upgrade.weaponUpgrade.range, "<sprite=2>");
            info += AddStatInfoText(upgrade.weaponUpgrade.angle, "<sprite=4>");
            info += AddStatInfoText(upgrade.weaponUpgrade.knockback, "<sprite=3>");
            info += AddStatInfoText(upgrade.weaponUpgrade.speed, "<sprite=1>");
            info += AddStatInfoText(upgrade.weaponUpgrade.stuntime, "<sprite=5>");
        }

        if (upgrade.playerUpgrade.health != 0 || upgrade.playerUpgrade.speed != 0 || 
            upgrade.playerUpgrade.dashDistance != 0)
        {
            info += "Joueur :\n";

            info += AddStatInfoText(upgrade.playerUpgrade.health, "<sprite=8>");
            info += AddStatInfoText(upgrade.playerUpgrade.speed, "<sprite=6>");
            info += AddStatInfoText(upgrade.playerUpgrade.dashDistance, "<sprite=7>");
        }
        
        textInfo.text = info;
        textInfoSizeFitter.text = info;
    }

    public string AddStatInfoText(int value, string name)
    {
        string statInfo = "";
        
        if (value != 0)
        {
            statInfo += name + " ";
            if (value > 0) statInfo += "+";
            statInfo += value + "\n";
        }

        return statInfo;
    }

    public void Update()
    {
        // Detect if player is not selecting object
        if (timeBeforeClose > 0) timeBeforeClose -= Time.deltaTime;
        else showInfo = false;
        
        // Show info of object
        if (showInfo)
        {
            spriteRenderer.material.SetFloat("_OutlineThickness", outlineThickness);
            infoUI.SetActive(true);
        }
        else
        {
            spriteRenderer.material.SetFloat("_OutlineThickness", 0);
            infoUI.SetActive(false);
        }
    }

    public void Interact()
    {
        FindObjectOfType<PlayerController>().AddUpgrade(upgrade);
        FindObjectOfType<NewItemPanelScript>().TriggerPanel(upgrade);
        Destroy(gameObject);
    }

    public void Detect()
    {
        timeBeforeClose = Time.deltaTime*2;
        showInfo = true;
    }
}
