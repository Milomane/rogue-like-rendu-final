﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Button : MonoBehaviour
{
    public GameObject closeSetting, settingPanel, panelPause, gameover, helpStats, gameManager;
    public static bool GameIsPaused = false;
    
    private InputManager _inputManager;

    public void Start()
    {
        _inputManager = GameObject.FindObjectOfType<InputManager>();
        gameManager = GameObject.Find("Map");
    }

    void Update() // Appuyer sur la touche Echap pour mettre le jeu en pause
    {
        if ((_inputManager.GetButtonDown("Pause") || Input.GetKeyDown(KeyCode.JoystickButton7)) && !gameover.activeInHierarchy)
        {
            if (GameIsPaused)
                Resume();
            else
                Pause();
        }
    }
    
    public void Resume() // Remet le jeu en route
    {
        panelPause.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;
    }

    void Pause() // Arrete le temps du jeu
    {
        panelPause.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
    }
    
    public void LoadMenu() // retourne au Menu
    {
        Time.timeScale = 1f;
        FindObjectOfType<Restart>().DestroyDoubleMenu();
        GameIsPaused = false;
        SceneManager.LoadScene(0);
    }
    
    public void Play() //lance le jeu
    {
        if (PlayerPrefs.GetFloat("AlreadyStarted") == 1)
        {
            SceneManager.LoadScene(1);
        }
        else
        {
            PlayerPrefs.SetFloat("AlreadyStarted", 1);
            SceneManager.LoadScene(2);
        }
    }

    public void Setting() //ouvre la fenetre de parametre
    {
        closeSetting.SetActive(true);
        settingPanel.SetActive(true);
    }

    public void QuitSetting() //quitte les parametre
    {
        closeSetting.SetActive(false);
        settingPanel.SetActive(false);
    }

    public void Reset()
    {
        _inputManager.GetComponent<InputManager>().Reset();
    }

    public void Restart()
    {
        Time.timeScale = 1f;
        GameIsPaused = false;
        FindObjectOfType<Restart>().DestroyDoubleRestart();
    }

    public void Help()
    {
        helpStats.SetActive(!helpStats.activeSelf);
    }
    
    public void Quit() //quitte le jeu
    {
        Application.Quit();
        Debug.Log("Quit Game");
    }
}