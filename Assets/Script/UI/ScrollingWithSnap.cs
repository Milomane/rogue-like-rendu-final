﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollingWithSnap : MonoBehaviour
{
    private GameObject[] itemList;
    private int index;

    void Start()
    {
        index = PlayerPrefs.GetInt("ItemsSelected"); //pour l'item sélectionné
    }

    void Update()
    {
        itemList = new GameObject[transform.childCount];

        for (int i = 0; i < transform.childCount; i++) //remplir le tableau avec les items
            itemList[i] = transform.GetChild(i).gameObject;
    }
    
    public  void ToggleUp() //pour la flèche vers le haut
    {
        itemList[index].GetComponent<Image>().color = Color.blue;

        index--; //on recule dans l'index
        if (index < 0)
            index = itemList.Length - 1;
        
        itemList[index].GetComponent<Image>().color = Color.magenta;
    }
    
    public  void ToggleDown() //pour la flèche vers le bas
    {
        itemList[index].GetComponent<Image>().color = Color.blue;

        index++; //on avance dans l'index
        if (index == itemList.Length)
            index = 0;
        
        itemList[index].GetComponent<Image>().color = Color.magenta;
    }
}