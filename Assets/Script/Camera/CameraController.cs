﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Vector3 originalPos;
    public float antiShake;
    
    void Start()
    {
        Cursor.lockState = CursorLockMode.Confined;
        originalPos = transform.localPosition;
    }
    void Update()
    {
        transform.localPosition = Vector3.Lerp(transform.localPosition, originalPos, antiShake);
    }

    public void Shake(float duration, float strenght, int vibrato, float randomness)
    {
        transform.DOShakePosition(duration, strenght, vibrato, randomness);
    }
}
