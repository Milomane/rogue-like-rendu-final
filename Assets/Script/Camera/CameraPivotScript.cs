﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPivotScript : MonoBehaviour
{
    public GameObject target;
    
    [Range(0.00f, 1.00f)]
    public float forcePercentage;

    public float cameraSpeed;

    public Vector3 targetPos;
    void Start()
    {
        target = FindObjectOfType<PlayerController>().gameObject;
    }
    void Update()
    {
        if (FindObjectOfType<PlayerController>().playerAsControl)
        {
            if (Time.deltaTime > 0)
            {
                Vector3 mousePos = Input.mousePosition;
                mousePos.z = Camera.main.nearClipPlane;
                Vector3 mouseWorldPosition = Camera.main.ScreenToWorldPoint(mousePos);

                targetPos = target.transform.position + (mouseWorldPosition - target.transform.position) * forcePercentage;

                transform.position = Vector3.Lerp(transform.position, new Vector3(targetPos.x, targetPos.y, transform.position.z), cameraSpeed);
            }
        }
        else
        {
            if (target != null)
            {
                targetPos = target .transform.position;
                transform.position = Vector3.Lerp(transform.position, new Vector3(targetPos.x, targetPos.y, transform.position.z), cameraSpeed);
            }
        }
    }

    public void TargetPlayer()
    {
        target = FindObjectOfType<PlayerController>().gameObject;
    }

    public void SpecificTarget(GameObject newTarget)
    {
        target = newTarget;
    }
}
