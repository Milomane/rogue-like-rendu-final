﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomDecor : MonoBehaviour
{
    public DecoPool decoPool;
    private SpriteRenderer spriteRenderer;
    
    void Start()
    {
        if (Random.Range(0f, 1f) > .25f)
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
            spriteRenderer.sprite = decoPool.pool[(int)Random.Range(0, decoPool.pool.Count)];
        }
    }
}
