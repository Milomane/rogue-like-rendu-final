﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
    private List<AudioScript> allAudioScript;

    public AudioMixerGroup masterMixer;
    public AudioMixerGroup ennemysMixer;
    public AudioMixerGroup effectsMixer;
    public AudioMixerGroup mucicsMixer;

    public float masterMixerVolume;
    public float ennemysMixerVolume;
    public float effectsMixerVolume;
    public float musicsMixerVolume;

    public float demultiplierPerAudio = .75f;

    public void Start()
    {
        masterMixer.audioMixer.GetFloat("Master", out masterMixerVolume);
        ennemysMixer.audioMixer.GetFloat("Enemy", out ennemysMixerVolume);
        effectsMixer.audioMixer.GetFloat("Effects", out effectsMixerVolume);
        mucicsMixer.audioMixer.GetFloat("Music", out musicsMixerVolume);
        
        allAudioScript = new List<AudioScript>();
    }

    void Update()
    {
        int ennemyAudioCount = 0;
        int effectAudioCount = 0;
        int musicAudioCount = 0;

        for (int i = 0; i < allAudioScript.Count; i++)
        {
            switch (allAudioScript[i].audioMixerGroup.name)
            {
                case "Music":
                    musicAudioCount++;
                    break;
                case "Enemy":
                    ennemyAudioCount++;
                    break;
                case "Effects":
                    effectAudioCount++;
                    break;
            }
        }
        /*
        ennemysMixer.audioMixer.SetFloat("Enemy", -80 + (80 + ennemysMixerVolume) / (ennemyAudioCount * demultiplierPerAudio));
        effectsMixer.audioMixer.SetFloat("Effects", -80 + (80 + effectsMixerVolume) / (effectAudioCount * demultiplierPerAudio));
        mucicsMixer.audioMixer.SetFloat("Music", -80 + (80 + musicsMixerVolume) / (musicAudioCount * demultiplierPerAudio));
        
        Debug.Log(-80 + (80 + effectsMixerVolume) / (effectAudioCount * demultiplierPerAudio));
        
        //Debug.Log(-80 + (80 - ennemysMixerVolume) / (ennemyAudioCount * demultiplierPerAudio));
        */
    }
    
    public void AddNewSound(AudioScript audioScript, AudioClip audioClip)
    {
        StartCoroutine(AddNewSoundC(audioScript, audioClip));
    }

    public IEnumerator AddNewSoundC(AudioScript audioScript, AudioClip audioClip)
    {
        allAudioScript.Add(audioScript);
        Debug.Log("Yolo " + allAudioScript.Count + "    " + audioClip.length);
        
        yield return new WaitForSeconds(audioClip.length);
        allAudioScript.Remove(audioScript);
    }
}
