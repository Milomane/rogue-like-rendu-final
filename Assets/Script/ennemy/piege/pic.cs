﻿using System;
using System.Collections;
using System.Collections.Generic;
using AudioAutoPlayerSpace;
using UnityEngine;
using UnityEngine.Audio;
using Random = UnityEngine.Random;

public class pic : MonoBehaviour
{
    public float triggerTime = 1f;
    private float timerPic,timerPicEnemy;
    public float knockbackPower = 5f;
    public float picCooldown = 1f;
    private float picTimerCD;
    private bool picDamage, picDamageEnemy;
    
    private Animator anim;
    private bool actifPic, soundActifPic;
    private bool playerIn;
    private GameObject _player;
    public Vector3 position;
    public AudioClip soundPiegePic;
    public AudioMixerGroup mixerEnnemy;

    private void Start()
    {
        anim = GetComponent<Animator>();
        _player = FindObjectOfType<PlayerController>().gameObject;
    }

    void Update()
    {
        if (actifPic)
        {
            timerPic += Time.deltaTime;
            if (timerPic >= 0)
            {
                anim.SetFloat("Attack",0);
                anim.SetFloat("prepaAttack",1);
                if (timerPic > 0.2f)
                {
                    if (soundActifPic)
                    {
                        soundActifPic = false;
                        AudioAutoPlayer.Play(soundPiegePic, transform.position, mixerEnnemy,0.3f, Random.Range(0.8f, 1.2f));
                    } 
                }
                
            }
            if (timerPic >= triggerTime)
            {
                anim.SetFloat("Attack",1);
                StartCoroutine(DamagePlayer(.5f));
                
                picTimerCD = picCooldown;
                timerPic = 0;
                actifPic = false;
            }
        }
        else if(timerPicEnemy >= 0.1f && !picDamageEnemy)
        {
            anim.SetFloat("prepaAttack",1);
        }
        else
        {
            anim.SetFloat("Attack",0);
            anim.SetFloat("prepaAttack",0);
        }
        
        if (picDamage)
        {
            if (playerIn)
            {
                _player.GetComponent<PlayerHealth>().TakeDamage(1, knockbackPower, transform.position);
                picDamage = false;
            }
        }

        picTimerCD -= Time.deltaTime;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("PlayerFeet"))
        {
            if (picTimerCD <= 0)
            {
                soundActifPic = true;
            }
        }
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("PlayerFeet"))
        {
            _player = other.GetComponentInParent<PlayerController>().gameObject;
            playerIn = true;
            
            if (picTimerCD <= 0)
            {
                actifPic = true;
            }
        }
        if (other.gameObject.CompareTag("Enemy"))
        {
            timerPicEnemy += Time.deltaTime;
            
            if (timerPicEnemy >= triggerTime)
            {
                if (!picDamageEnemy)
                {
                    position = transform.position;
                    picDamageEnemy = true;
                    anim.SetFloat("prepaAttack",0);
                    anim.SetFloat("Attack",1);
                    AudioAutoPlayer.Play(soundPiegePic, transform.position, mixerEnnemy,0.55f, Random.Range(0.8f, 1.2f));
                    other.gameObject.GetComponent<ennemyController>().TakeDamage(1,position,0.5f,knockbackPower);
                }
            }
        }
    }
    
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("PlayerFeet"))
        {
            playerIn = false;
        }

        if (other.gameObject.CompareTag("Enemy"))
        {
            picDamageEnemy = false;
            timerPicEnemy = 0f;
            StartCoroutine(DamageEnemy());
        }
    }

    public IEnumerator DamagePlayer(float time)
    {
        picDamage = true;
        yield return new WaitForSeconds(time);
        picDamage = false;
    }

    public IEnumerator DamageEnemy()
    {
        yield return new WaitForSeconds(0.3f);
        anim.SetFloat("Attack",0);
    }
}
