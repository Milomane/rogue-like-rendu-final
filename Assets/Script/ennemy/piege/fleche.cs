﻿using System.Collections;
using System.Collections.Generic;
using AudioAutoPlayerSpace;
using UnityEngine;
using UnityEngine.Audio;

public class fleche : MonoBehaviour
{
    public GameObject firePoint1;

    public GameObject flecheProjectil;

    public float timerFleche;
    private Animator anim;
    public AudioClip flecheSound;
    public AudioMixerGroup mixerEnnemy;

    private void Start()
    {
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        timerFleche += Time.deltaTime;
        anim.SetFloat("Attack",0);
        if (timerFleche >= 1)
        {
            anim.SetFloat("Attack",1);
        }

        if (timerFleche >= 2.5f)
        {
            AudioAutoPlayer.Play(flecheSound, transform.position, mixerEnnemy,0.1f, Random.Range(0.8f, 1.2f)); 
            Instantiate(flecheProjectil,firePoint1.transform.position,firePoint1.transform.rotation);
            timerFleche = 0;
        }
    }
}
