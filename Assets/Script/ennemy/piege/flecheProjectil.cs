﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class flecheProjectil : MonoBehaviour
{
    public int speed;
    public int colliderDetectionWall;
    public float knockbackPower = 5f;
    public Vector3 position;
    
    void Update()
    {
        transform.Translate(Vector3.down * speed * Time.deltaTime );
        if (colliderDetectionWall == 2)
        {
            Destroy(gameObject);
        }
    }
    

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            other.gameObject.GetComponent<PlayerHealth>().TakeDamage(1, knockbackPower, transform.position);
            Destroy(gameObject);
        }

        if (other.gameObject.CompareTag("Wall"))
        {
            colliderDetectionWall++;
        }

        if (other.gameObject.CompareTag("Enemy"))
        {
            position = gameObject.transform.position;
            other.gameObject.GetComponent<ennemyController>().TakeDamage(2,position,1,2);
            Destroy(gameObject);
        }
    }
}
