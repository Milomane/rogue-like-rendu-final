﻿using System.Collections;
using System.Collections.Generic;
using AudioAutoPlayerSpace;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class ennemyController : MonoBehaviour
{
    public float health, maxHealth, stunTimer, knockbackResistance;
    private bool stunActif, knockbakcActif, ennemyCaster, ennemyTrash, ennemyTank, ennemySouffleur, ennemyDamocles;
    public GameObject _player;
    public Image healthBarEnemy;

    private Rigidbody2D _rigidbody2D;
    private Animator _animator;

    public AudioClip hurtSoundTank, hurtSoundCaster, hurtSoundTrash, hurtSoundSouffle, deathTank, deathTrash, deathCaster, deathSouffle;
    public AudioMixerGroup mixerEnnemy;

    public GameObject lootPrefab,lootLifePrefab;
    public float lootProbability;
    public int lootMax;
    private Animator anim;
    private bool dead;
    private SpriteRenderer spriteRenderer;
    public Color colorDamage = Color.red;
    
    void Start()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();
        health = maxHealth;//vie = max vie
        anim = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();

        _player = GameObject.FindWithTag("Player");
        //check script attaché à l'ennemy
        if (gameObject.GetComponent<caster>() == true)
        {
            ennemyCaster = true;
        }
        if (gameObject.GetComponent<ennemyDamocles>() == true)
        {
            ennemyDamocles = true;
        }
        if (gameObject.GetComponent<ennemySouffle>() == true)
        {
            ennemySouffleur = true;
        }
        if (gameObject.GetComponent<ennemyTrash>() == true)
        {
            ennemyTrash = true;
        }
        if (gameObject.GetComponent<ennemyTank>() == true)
        {
            ennemyTank = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        float yPos = -transform.position.y * 100;
        GetComponent<SpriteRenderer>().sortingOrder = (int)yPos;
        
        //gestion de la vie de l'ennemy
        if (health > maxHealth)
        {
            health = maxHealth;
        }
        if (health <= 0)
        {
            if (!dead)
            {
                dead = true;
                if (Random.Range(0f, 1f) < lootProbability)
                {
                    int coinCount = Random.Range(0, lootMax);
                    for (int i = 0; i < coinCount; i++)
                    {
                        Instantiate(lootPrefab, transform.position, Quaternion.identity);
                    }
                }
                int regenLife = Random.Range(0,10);
                if (regenLife == 9)
                {
                    Instantiate(lootLifePrefab, transform.position, Quaternion.identity);
                }
                anim.SetFloat("Death",1);
                //gestion sound mort de l'enemy
                if (ennemyCaster)
                {
                    AudioAutoPlayer.Play(deathCaster, transform.position, mixerEnnemy,0.55f, Random.Range(0.8f, 1.2f));
                }
                if (ennemySouffleur)
                {
                    AudioAutoPlayer.Play(deathSouffle, transform.position, mixerEnnemy,0.55f, Random.Range(0.8f, 1.2f));
                }
                if (ennemyTrash)
                {
                    AudioAutoPlayer.Play(deathTrash, transform.position, mixerEnnemy,0.55f, Random.Range(0.8f, 1.2f));
                }
                if (ennemyTank)
                {
                    AudioAutoPlayer.Play(deathTank, transform.position, mixerEnnemy,0.55f, Random.Range(0.8f, 1.2f)); 
                }
                StartCoroutine(AnimDeath());
            }
        }

        //Timer stun de l'ennemy
        if (stunActif)
        {
            stunTimer -= Time.deltaTime;
            if (stunTimer <= 0)
            {
                stunActif = false;
            }
        }

        //réactivation movement ennemy après stun
        if (!stunActif)
        {
            if (ennemyCaster)
            {
                gameObject.GetComponent<caster>().stunActif = false;
            }
            if (ennemyTrash)
            {
                gameObject.GetComponent<ennemyTrash>().stunActif = false;
            }
            if (ennemyTank)
            {
                gameObject.GetComponent<ennemyTank>().stunActif = false;
            }
            if (ennemySouffleur)
            {
                gameObject.GetComponent<ennemySouffle>().stunActif = false;
            }
            //rajouter les autres types d'ennemis
        }
    }

    public void TakeDamage(float valueDamage, Vector3 playerPosition, float valueStun, float powerKnockback)
    {
        if (stunActif)
        {
            stunActif = false;
        }
        stunTimer = valueStun;
        stunActif = true;
        
        // Knockback
        Knockback(playerPosition, powerKnockback);
        
        // Play sound damage suivant enemy
        if (ennemyCaster)
        {
            AudioAutoPlayer.Play(hurtSoundCaster, transform.position, mixerEnnemy,0.55f, Random.Range(0.8f, 1.2f));
        }
        if (ennemySouffleur)
        {
            AudioAutoPlayer.Play(hurtSoundSouffle, transform.position, mixerEnnemy,0.55f, Random.Range(0.8f, 1.2f));
        }
        if (ennemyTrash)
        {
            AudioAutoPlayer.Play(hurtSoundTrash, transform.position, mixerEnnemy,0.55f, Random.Range(0.8f, 1.2f));
        }
        if (ennemyTank)
        {
            AudioAutoPlayer.Play(hurtSoundTank, transform.position, mixerEnnemy,0.55f, Random.Range(0.8f, 1.2f)); 
        }
        
        
        StunEnnemy();
        healthBarEnemy.fillAmount -= valueDamage/maxHealth;
        health -= valueDamage;
        
        // Anim damage
        if (health <= 0)
        {
            StartCoroutine(RedAnim());  
            StartCoroutine(AnimDeath());
        }
        else
        {
            StartCoroutine(RedAnim());  
            StartCoroutine(AnimDamage(valueStun));  
        }
    }

    public void Knockback(Vector3 originPos, float power)
    {
        Vector3 knockbackDirection = (transform.position - originPos).normalized;
        _rigidbody2D.AddForce(knockbackDirection * power * knockbackResistance, ForceMode2D.Impulse);
    }

    public IEnumerator AnimDamage(float animTime)
    {
        if (ennemyTrash)
        {
            _animator.SetBool("Damage", true);
        }
        if (ennemyCaster)
        {
            _animator.SetBool("Damage", true);
        }
        if (ennemySouffleur)
        {
            _animator.SetBool("Damage", true);
        }
        if (ennemyTank)
        {
            
        }
        yield return new WaitForSeconds(0.5f);
        _animator.SetBool("Damage", false);
    }

    public IEnumerator RedAnim()
    {
        spriteRenderer.DOColor(colorDamage, .2f);
        transform.DOScale(Vector3.one * .9f, .2f);
        
        yield return new WaitForSeconds(0.2f);
        transform.DOScale(Vector3.one, .2f);
        spriteRenderer.DOColor(Color.white, .2f);
    }

    void StunEnnemy()
    {
        if (ennemyCaster)
        {
            gameObject.GetComponent<caster>().stunActif = true;
        }
        if (ennemyTrash)
        {
            gameObject.GetComponent<ennemyTrash>().stunActif = true;
        }
        if (ennemySouffleur)
        {
            gameObject.GetComponent<ennemySouffle>().stunActif = true;
        }
        if (ennemyTank)
        {
            gameObject.GetComponent<ennemyTank>().stunActif = true;
        }
    }

    public IEnumerator AnimDeath()
    {
        yield return new WaitForSeconds(0.6f);
        Destroy(gameObject);
    }
}
