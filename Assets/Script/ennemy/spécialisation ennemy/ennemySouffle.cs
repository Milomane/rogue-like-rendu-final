﻿using System.Collections;
using System.Collections.Generic;
using AudioAutoPlayerSpace;
using UnityEngine;
using Random = UnityEngine.Random;
using Pathfinding;
using UnityEngine.Audio;

public class ennemySouffle : MonoBehaviour
{
    private Vector3 startingPosition;

    public float distanceWaypoint, distanceDetection,speed;
    private float timerAttack, numbAttack,resetNumbAttack;
    public bool stunActif, attackActif;
    public GameObject _player, missileEnnemy;
    private Path path;
    private int waypoint = 0;
    private bool finishPath = false;
    private Rigidbody2D rbEnemy;
    private Seeker _seeker;
    private Animator Anim;
    public AudioClip soundTireEnemy;
    public AudioMixerGroup mixerEnnemy;
    



    // Start is called before the first frame update
    void Start()
    {
        startingPosition = transform.position;
        _player = GameObject.FindGameObjectWithTag("Player");
        rbEnemy = gameObject.GetComponent<Rigidbody2D>();
        _seeker = gameObject.GetComponent<Seeker>();
        Anim = GetComponent<Animator>();

        InvokeRepeating("UpdatePath", 0f, 0.5f);
    }

    void UpdatePath()
    {
        if (_seeker.IsDone())
        {
            _seeker.StartPath(rbEnemy.position, _player.transform.position, PathCompleted);
        }
        
    }
    private void PathCompleted(Path p)
    {
        if (!p.error)
        {
            path = p;
            waypoint = 0;
        }
    }
    private Vector3 RandomPositionEnemy()
    {
        Vector3 positionEnemy;
        return startingPosition + (positionEnemy = new Vector3(UnityEngine.Random.Range(-1f,1f), UnityEngine.Random.Range(-1f,1f)).normalized) * Random.Range(10f , 10f) ;
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        if (!stunActif)
        {
            //mouvement de l'enemy à partir d'une détection autour pour voir si le player est dans la zone
            RaycastHit2D[] raycastDetection = Physics2D.CircleCastAll(transform.position, distanceDetection, transform.position);
            foreach (var hit in raycastDetection)
            {
                //si player detection = true alors enemy go sur lui
                if (hit.collider.CompareTag("Player"))
                {
                    if (path == null)
                    {
                        return;
                    }

                    if (waypoint >= path.vectorPath.Count)
                    {
                        finishPath = true;
                        return;
                    }
                    else
                    {
                        finishPath = false;
                    }

                    Vector2 direction = ((Vector2) path.vectorPath[waypoint] - rbEnemy.position).normalized;
                    Vector2 force = direction * speed * Time.deltaTime;
            
                    rbEnemy.AddForce(force);

                    Anim.SetFloat("RunX", direction.x);
                    Anim.SetFloat("RunY", direction.y);

                    float distance = Vector2.Distance(rbEnemy.position, path.vectorPath[waypoint]);
                    if (distance < distanceWaypoint)
                    {
                        waypoint++;
                    }
                } 
            }
            attackActif = false;

            RaycastHit2D[] raycastDetectionAttack = Physics2D.CircleCastAll(transform.position, 4f, _player.transform.position);
            foreach (var hit in raycastDetectionAttack)
            {
                if (hit.collider.CompareTag("Player"))
                {
                    attackActif = true;
                }
            }

            if (attackActif)
            {
                if (numbAttack <= 6f)
                {
                    timerAttack += Time.deltaTime;
                    if (timerAttack > 0.3f) 
                    {
                        Anim.SetFloat("Attack",1);
                        
                        StartCoroutine(Attack());
                        if (timerAttack > 0.6f)
                        {
                            Instantiate(missileEnnemy, transform.position, transform.rotation);
                            AudioAutoPlayer.Play(soundTireEnemy, transform.position, mixerEnnemy,0.20f, Random.Range(0.8f, 1.2f));
                            timerAttack = 0f;
                            numbAttack++;
                        }
                    }
                }

                if (numbAttack >= 7f)
                {
                    resetNumbAttack += Time.deltaTime;
                    if (resetNumbAttack > 2.5f)
                    {
                        numbAttack = 0f;
                        resetNumbAttack = 0f;
                    }
                }
            }
        }
    }

    private IEnumerator Attack()
    {
        yield return new WaitForSeconds(0.3f);
        Anim.SetFloat("Attack",0);
    }
}
