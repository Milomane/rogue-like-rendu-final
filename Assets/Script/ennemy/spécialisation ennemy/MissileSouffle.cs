﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileSouffle : MonoBehaviour
{
    public GameObject _player;
    public float speedMissile;
    public float knockbackPower = 5f;
    private Vector2 playerPosition;
    
    private Rigidbody2D rb;
    
    // Start is called before the first frame update
    void Start()
    {
        _player = GameObject.FindWithTag("Player");
        rb = gameObject.GetComponent<Rigidbody2D>();
        playerPosition = (_player.transform.position - transform.position).normalized;
        
    }
    // Update is called once per frame
    void Update()
    {
        rb.AddForce(playerPosition * speedMissile);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Wall"))
        {
            Destroy(gameObject);
        }

        if (other.CompareTag("Player"))
        {
            _player.GetComponent<PlayerHealth>().TakeDamage(1, knockbackPower, transform.position);
            Destroy(gameObject);
        }
    }
}
