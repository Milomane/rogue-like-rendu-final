﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackTrash : MonoBehaviour
{
    public GameObject _player;
    public int attackTrashValue;
    
    public float knockbackPower = 5f;
    
    
    private void Start()
    {
        _player = GameObject.FindWithTag("Player");
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            _player.GetComponent<PlayerHealth>().TakeDamage(attackTrashValue, knockbackPower, transform.position);
            gameObject.SetActive(false);
        }
    }
}
