﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;
using Pathfinding;

public class ennemyDamocles : MonoBehaviour
{
    private Vector3 startingPosition;

    public float distanceWaypoint, distanceDetection;
    public float speed;
    public bool stunActif;
    public GameObject _player;
    private Path path;
    private int waypoint = 0;
    private bool finishPath = false;
    private Rigidbody2D rbEnemy;
    private Seeker _seeker;
    public float timerNextAttack;
    private Animator Anim;
    
    public float knockbackPower = 5f;
    
    // Start is called before the first frame update
    void Start()
    {
        startingPosition = transform.position;
        _player = GameObject.FindGameObjectWithTag("Player");
        rbEnemy = gameObject.GetComponent<Rigidbody2D>();
        _seeker = gameObject.GetComponent<Seeker>();
        Anim = GetComponent<Animator>();

        InvokeRepeating("UpdatePath", 0f, 0.5f);
    }
    void UpdatePath()
    {
        if (_seeker.IsDone())
        {
            _seeker.StartPath(rbEnemy.position, _player.transform.position, PathCompleted);
        }
        
    }
    private void PathCompleted(Path p)
    {
        if (!p.error)
        {
            path = p;
            waypoint = 0;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!stunActif)
        {
            Anim.SetFloat("Attack",0f);
            RaycastHit2D[] raycastDetection = Physics2D.CircleCastAll(transform.position, distanceDetection, transform.position);
            foreach (var hit in raycastDetection)
            {
                if (hit.collider.CompareTag("Player"))
                {
                    Anim.SetFloat("Attack",1f);
                    if (path == null)
                    {
                        return;
                    }

                    if (waypoint >= path.vectorPath.Count)
                    {
                        finishPath = true;
                        return;
                    }
                    else
                    {
                        finishPath = false;
                    }

                    Vector2 direction = ((Vector2) path.vectorPath[waypoint] - rbEnemy.position).normalized;
                    Vector2 force = direction * speed * Time.deltaTime;
                    
                    rbEnemy.AddForce(force);

                    float distance = Vector2.Distance(rbEnemy.position, path.vectorPath[waypoint]);
                    if (distance < distanceWaypoint)
                    {
                        waypoint++;
                    }
                }
            }
        }
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Debug.Log("damocles touche player");
            timerNextAttack += Time.deltaTime;
            if (timerNextAttack > 1.5f)
            {
                other.gameObject.GetComponent<PlayerHealth>().TakeDamage(1, knockbackPower, transform.position);
                timerNextAttack = 0f;
            }
            
        }
    }
}
