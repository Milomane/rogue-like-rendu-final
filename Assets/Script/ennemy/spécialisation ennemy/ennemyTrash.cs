﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;
using Pathfinding;

public class ennemyTrash : MonoBehaviour
{
    private Vector3 startingPosition;

    public float distanceWaypoint, distanceDetection;
    public float minAttackDistance;
    public float attackCooldown = 1.5f;
    public float speed;
    public bool stunActif;
    public GameObject _player;
    private Path path;
    private int waypoint = 0;
    private bool finishPath = false;
    private Rigidbody2D rbEnemy;
    private Seeker _seeker;
    public float timerNextAttack;
    public GameObject colliderBoxAttack;
    private Animator Anim;



    // Start is called before the first frame update
    void Start()
    {
        startingPosition = transform.position;
        _player = GameObject.FindGameObjectWithTag("Player");
        rbEnemy = gameObject.GetComponent<Rigidbody2D>();
        _seeker = gameObject.GetComponent<Seeker>();
        Anim = GetComponent<Animator>();
        

        InvokeRepeating("UpdatePath", 0f, 0.5f);
    }

    void UpdatePath()
    {
        if (_seeker.IsDone())
        {
            _seeker.StartPath(rbEnemy.position, _player.transform.position, PathCompleted);
        }
        
    }
    private void PathCompleted(Path p)
    {
        if (!p.error)
        {
            path = p;
            waypoint = 0;
        }
    }
    
    
    // Update is called once per frame
    void Update()
    {
        if (timerNextAttack >= 0)
        {
            timerNextAttack -= Time.deltaTime;
        }

        if (!stunActif)
        {
            RaycastHit2D[] raycastAttack = Physics2D.CircleCastAll(transform.position, minAttackDistance, transform.position);
            foreach (var hit in raycastAttack)
            {
                if (hit.collider.CompareTag("Player"))
                {
                    if (timerNextAttack <= 0f)
                    {
                        timerNextAttack = attackCooldown;
                        StartCoroutine(Attack());
                    }
                }
            }
        }
    }

    private void FixedUpdate()
    {
        if (path == null)
        {
            return;
        }

        if (waypoint >= path.vectorPath.Count)
        {
            finishPath = true;
            return;
        }
        else
        {
            finishPath = false;
        }

        Vector2 direction = ((Vector2) path.vectorPath[waypoint] - rbEnemy.position).normalized;
        Vector2 force = direction * speed * Time.deltaTime;
                    
        rbEnemy.AddForce(force);
                    
        //gestion animation run
        Anim.SetFloat("MoveX", direction.x);
        Anim.SetFloat("MoveY", direction.y);

        float distance = Vector2.Distance(rbEnemy.position, path.vectorPath[waypoint]);
        if (distance < distanceWaypoint)
        {
            waypoint++;
        }
    }

    private IEnumerator Attack()
    {
        Anim.SetBool("Attack", true);
        yield return new WaitForSeconds(0.35f);
        
        colliderBoxAttack.SetActive(true);
        yield return new WaitForSeconds(0.01f);
        
        Anim.SetBool("Attack", false);
        colliderBoxAttack.SetActive(false);
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, minAttackDistance);
    }
}
