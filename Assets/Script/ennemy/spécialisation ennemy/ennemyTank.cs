﻿using System;
using System.Collections;
using System.Collections.Generic;
using AudioAutoPlayerSpace;
using UnityEngine;
using Random = UnityEngine.Random;
using Pathfinding;
using UnityEngine.Audio;
using UnityEngine.PlayerLoop;

public class ennemyTank : MonoBehaviour
{
    private Vector3 startingPosition;

    public float nextWaypointDistance = .05f;
    public float wallDistanceDetection = 1;
    
    public float speed;
    public float chargeSpeed;
    
    public bool stunActif;
    public GameObject _player;
    public LayerMask playerDetectionLayer;
    public LayerMask wallDetectionLayer;

    public float attackRadius = 1;
    public int attackDamage = 1;
    public float knockback = 5;

    private Path path;
    private int waypoint = 0;
    private bool finishPath = false;
    private Rigidbody2D rbEnemy;
    private Seeker _seeker;
    private Animator _animator;

    private float cooldown;
    public float timerNextAttack;

    public bool charging;
    private Vector2 vectorCharging;
    
    public AudioClip soundContactWall;
    public AudioMixerGroup mixerEnnemy;
    
    // Start is called before the first frame update
    void Start()
    {
        startingPosition = transform.position;
        _player = FindObjectOfType<PlayerController>().gameObject;
        rbEnemy = GetComponent<Rigidbody2D>();
        _seeker = GetComponent<Seeker>();
        _animator = GetComponent<Animator>();

        cooldown = timerNextAttack;
        
        InvokeRepeating("UpdatePath", 0f, 0.5f);
    }
    void UpdatePath()
    {
        if (_seeker.IsDone())
        {
            _seeker.StartPath(rbEnemy.position, _player.transform.position, PathCompleted);
        }
    }
    private void PathCompleted(Path p)
    {
        if (!p.error)
        {
            path = p;
            waypoint = 0;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!stunActif)
        {
            RaycastHit2D hit;
            Vector2 vectorToPlayer = (_player.transform.position - transform.position).normalized;
            hit = Physics2D.Raycast(transform.position, vectorToPlayer, Single.PositiveInfinity, playerDetectionLayer);
            
            if (hit.collider.GetComponent<PlayerController>())
            {
                Debug.Log("DetectPlayer");
                if (!charging && cooldown <= 0)
                {
                    charging = true;
                    vectorCharging = (_player.transform.position - transform.position).normalized;
                }
            }

            cooldown -= Time.deltaTime;
        }

        if (charging)
        {
            _animator.SetBool("Attack", true);
            _animator.SetFloat("DirX", vectorCharging.x);
            _animator.SetFloat("DirY", vectorCharging.y);
            
            Vector2 force = vectorCharging * chargeSpeed * Time.deltaTime;
            rbEnemy.AddForce(force);
            
            RaycastHit2D[] hits;
            hits = Physics2D.CircleCastAll(transform.position, 1f, vectorCharging, wallDistanceDetection, wallDetectionLayer);
            
            foreach (var hit in hits)
            {
                if (hit.collider.tag == "Wall")
                {
                    AudioAutoPlayer.Play(soundContactWall, transform.position, mixerEnnemy,0.30f, Random.Range(0.8f, 1.2f));
                    charging = false;
                    cooldown = timerNextAttack;
                    Camera.main.GetComponent<CameraController>().Shake(.3f, 1, 1, 45);
                }
            }

            RaycastHit2D playerHit;
            playerHit = Physics2D.CircleCast(transform.position, attackRadius, Vector2.up, attackRadius,
                playerDetectionLayer);
            if (playerHit.collider.GetComponent<PlayerHealth>())
            {
                playerHit.collider.GetComponent<PlayerHealth>().TakeDamage(attackDamage, knockback, transform.position);
            }
        }
        else
        {
            _animator.SetBool("Attack", false);
        }
    }

    void FixedUpdate()
    {
        if (!charging)
        {
            if (path == null)
            {
                return;
            }

            if (waypoint >= path.vectorPath.Count)
            {
                finishPath = true;
                return;
            }
            else
            {
                finishPath = false;
            }

            Vector2 direction = ((Vector2) path.vectorPath[waypoint] - rbEnemy.position).normalized;
            Vector2 force = direction * speed * Time.deltaTime;
        
            rbEnemy.AddForce(force);

            float distance = Vector2.Distance(rbEnemy.position, path.vectorPath[waypoint]);
            if (distance < nextWaypointDistance)
            {
                waypoint++;
            }
            
            _animator.SetFloat("DirX", direction.x);
            _animator.SetFloat("DirY", direction.y);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(new Vector2(transform.position.x, transform.position.y) + vectorCharging, 1f);
    }
}
