﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu]
public class Upgrade : ScriptableObject
{
    public Sprite sprite;
    public string itemName;
    public string itemDescription;
    
    public GeneralClass.WeaponUpgrade weaponUpgrade;
    public GeneralClass.PlayerUpgrade playerUpgrade;
}
