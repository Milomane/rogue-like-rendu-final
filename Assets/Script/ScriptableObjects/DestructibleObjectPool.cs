﻿using System.Collections;
using System.Collections.Generic;
using GeneralClass;
using UnityEngine;

[CreateAssetMenu]
public class DestructibleObjectPool : ScriptableObject
{
    public DestructibleObjectPoolA[] objectPool;
}