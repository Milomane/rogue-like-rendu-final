﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu]
public class ScriptableDestructibleObject : ScriptableObject
{
    public Sprite sprite;
    public Sprite brokenSprite;

    public Material particleMaterial;

    public GameObject loot;
    public int minLoot;
    public int maxLoot;
    public float lootProb;

    public AudioClip breakSound;
}
