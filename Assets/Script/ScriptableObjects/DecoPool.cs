﻿using System.Collections; 
using System.Collections.Generic; 
using GeneralClass; 
using UnityEngine; 
 
 
[CreateAssetMenu] 
public class DecoPool : ScriptableObject 
{ 
    public List<Sprite> pool; 
}