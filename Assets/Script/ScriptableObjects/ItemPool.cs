﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu]
public class ItemPool : ScriptableObject
{
    public List<GeneralClass.ShopItem> pool;
}