﻿using System.Collections; 
using System.Collections.Generic; 
using GeneralClass; 
using UnityEngine; 
 
 
[CreateAssetMenu] 
public class RoomPool : ScriptableObject 
{ 
    public List<GameObject> pool; 
}