﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ChangeScene : MonoBehaviour
{
    public string sceneToLoad;
    public float fadeTime = 1f;
    private Image fadeRenderer;

    private void Start()
    {
        fadeRenderer = GameObject.FindGameObjectWithTag("Fade").GetComponent<Image>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player") StartCoroutine(FadeOut());
    }

    public IEnumerator FadeOut()
    {
        fadeRenderer.DOFade(1, fadeTime);
        
        yield return new WaitForSeconds(fadeTime);

        SceneManager.LoadScene(sceneToLoad);
    }
}
