﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeartArray : MonoBehaviour
{
    public Image[] hearts;
    public GameObject gameOverScreen;
    public GameObject uniqueHeart;
}
