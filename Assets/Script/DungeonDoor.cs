﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonDoor : MonoBehaviour
{
    public bool open;

    public GameObject openObject;
    public GameObject closeObject;
    
    private BoxCollider2D boxCollider2D;
    
    void Start()
    {
        boxCollider2D = GetComponent<BoxCollider2D>();
    }
    
    void Update()
    {
        openObject.SetActive(open);
        closeObject.SetActive(!open);
        
        boxCollider2D.enabled = !open;
    }
}
