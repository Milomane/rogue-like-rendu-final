﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameplayManager : MonoBehaviour
{
    public bool debugMode;
    public TextMeshProUGUI debugText;

    public PlayerHealth playerHealth;
    public PlayerAttack playerAttack;
    public PlayerStats playerStats;
    
    public PlayerController playerController;

    public GameObject TrashPrefab, casterPrefab;

    public static bool create;

    public GameObject attaque, vitesse, portée, knockback, angle, stun, vitessePlayer, dash, coin;

    public bool activeDebugMode;
    void Start()
    {
        if (create)
        {
            Destroy(gameObject);
        }
        else
        {
            create = true;
            DontDestroyOnLoad(this);
        }
    }
    
    
    void Update()
    {
        attaque = GameObject.Find("attaque");
        vitesse = GameObject.Find("vitesse");
        portée = GameObject.Find("portée");
        knockback = GameObject.Find("knockback");
        angle = GameObject.Find("angle");
        stun = GameObject.Find("stun");
        vitessePlayer = GameObject.Find("vitessePlayer");
        dash = GameObject.Find("dash");
        coin = GameObject.Find("coin");
        
        playerHealth = FindObjectOfType<PlayerHealth>();
        playerAttack = FindObjectOfType<PlayerAttack>();
        playerStats = FindObjectOfType<PlayerStats>();
        playerController = FindObjectOfType<PlayerController>();
            
        //Player Stats UI
        attaque.GetComponent<TMP_Text>().text = "= " + playerAttack.attack;
        vitesse.GetComponent<TMP_Text>().text = "= " + playerAttack.speed;
        portée.GetComponent<TMP_Text>().text = "= " + playerAttack.range;
        knockback.GetComponent<TMP_Text>().text = "= " + playerAttack.knockback;
        angle.GetComponent<TMP_Text>().text = "= " + playerAttack.angle;
        stun.GetComponent<TMP_Text>().text = "= " + playerAttack.stunTime;
        vitessePlayer.GetComponent<TMP_Text>().text = "= " + playerStats.speed;
        dash.GetComponent<TMP_Text>().text = "= " + playerStats.dashDistance;
            
        coin.GetComponent<TMP_Text>().text = "= " + playerController.money;
        
        
        if (activeDebugMode)
        {
            // Active debug mode
            if (Input.GetKeyDown(KeyCode.Quote))
            {
                debugMode = !debugMode;
            }
            
            debugText = GameObject.FindGameObjectWithTag("DebugText").GetComponent<TextMeshProUGUI>();
            if (debugMode)
            {
                // Restart room
                if (Input.GetKeyDown(KeyCode.R))
                {
                    Application.LoadLevel(Application.loadedLevel);
                }
                
                // Lose health
                if (Input.GetKeyDown(KeyCode.N))
                {
                    playerHealth.TakeDamage(1, 0, Vector3.zero);
                }
                
                // PLAYER ATTACK
                debugText.text = "G- T+ Attack : " + playerAttack.attack
                                                   + "\nH- Y+ Attack Speed : " + playerAttack.speed
                                                   + "\nJ- U+ Range : " + playerAttack.range
                                                   + "\nK- I+ Knockback : " + playerAttack.knockback
                                                   + "\nL- O+ Angle : " + playerAttack.angle
                                                   + "\nM- P+ Stun Time : " + playerAttack.stunTime
                                                   
                                                   + "\n\nW- X+ Player health : " + playerStats.health
                                                   + "\nC- V+ Dash distance : " + playerStats.dashDistance
                                                   + "\nG- T+ Player speed : " + playerStats.speed

                                                   + "\n\nB- V+ Money : " + playerController.money

                                                   + "\n\nZQSD -> Déplacement"
                                                   + "\nEspace -> Dash"

                                                   + "\n\nClick gauche -> Attaque"
                                                   + "\nClick droit (rester appuyer) -> Attaque chargé"
                                                   + "\nN -> Lose health"
                                                   
                                                   + "\n\nEchap -> Menu pause" 
                                                   
                                                   + "\n\n1 -> Spawn Trash"
                                                   + "\n\n2 -> Spawn Caster";

                // Control stats
                if (Input.GetKeyDown(KeyCode.T))
                {
                    playerAttack.attack++;
                }
                if (Input.GetKeyDown(KeyCode.G))
                {
                    if(playerAttack.attack - 1 > 0) playerAttack.attack--;
                }
                if (Input.GetKeyDown(KeyCode.Y))
                {
                    playerAttack.speed++;
                }
                if (Input.GetKeyDown(KeyCode.H))
                {
                    if(playerAttack.speed - 1 > 0) playerAttack.speed--;
                }
                if (Input.GetKeyDown(KeyCode.U))
                {
                    playerAttack.range++;
                }
                if (Input.GetKeyDown(KeyCode.J))
                {
                    if(playerAttack.range - 1 > 0) playerAttack.range--;
                }
                if (Input.GetKeyDown(KeyCode.I))
                {
                    playerAttack.knockback++;
                }
                if (Input.GetKeyDown(KeyCode.K))
                {
                    if(playerAttack.knockback - 1 > 0) playerAttack.knockback--;
                }
                if (Input.GetKeyDown(KeyCode.O))
                {
                    playerAttack.angle++;
                }
                if (Input.GetKeyDown(KeyCode.L))
                {
                    if(playerAttack.angle - 1 > 0) playerAttack.angle--;
                }
                if (Input.GetKeyDown(KeyCode.P))
                {
                    playerAttack.stunTime++;
                }
                if (Input.GetKeyDown(KeyCode.M))
                {
                    if(playerAttack.stunTime - 1 > 0) playerAttack.stunTime--;
                }

                if (Input.GetKeyDown(KeyCode.X))
                {
                    playerStats.health++;
                }
                if (Input.GetKeyDown(KeyCode.W))
                {
                    if (playerStats.health - 1 > 0) playerStats.health--;
                }
                if (Input.GetKeyDown(KeyCode.T))
                {
                    playerStats.speed++;
                }
                if (Input.GetKeyDown(KeyCode.G))
                {
                    if (playerStats.speed - 1 > 0) playerStats.speed--;
                }
                if (Input.GetKeyDown(KeyCode.V))
                {
                    playerStats.dashDistance++;
                }
                if (Input.GetKeyDown(KeyCode.C))
                {
                    if (playerStats.dashDistance - 1 > 0) playerStats.dashDistance--;
                }
                
                // Spawn ennemy
                if (Input.GetKeyDown(KeyCode.Alpha1) || Input.GetKeyDown(KeyCode.Keypad1))
                {
                    GameObject player = FindObjectOfType<PlayerMovement>().gameObject;

                    Instantiate(TrashPrefab, player.transform.position + Vector3.up * 4, Quaternion.identity);
                }
                if (Input.GetKeyDown(KeyCode.Alpha2) || Input.GetKeyDown(KeyCode.Keypad2))
                {
                    GameObject player = FindObjectOfType<PlayerMovement>().gameObject;

                    Instantiate(casterPrefab, player.transform.position + Vector3.up * 4, Quaternion.identity);
                }
                
                // Control Money
                if (Input.GetKeyDown(KeyCode.V))
                {
                    playerController.money += 5;
                }

                if (Input.GetKeyDown(KeyCode.B))
                {
                    playerController.money -= 5;
                }
            }
            else
            {
                debugText.text = "² Pour activer / désactiver le debug mode";
            }
        }
    }
}
