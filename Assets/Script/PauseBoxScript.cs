﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PauseBoxScript : MonoBehaviour
{
    public TextMeshProUGUI text;
    public Image image;
    
    public GameObject indexButton;

    public Upgrade upgrade;

    public Vector3 targetPos;
    public float lerpSpeed;

    public bool select;
    private Animator animator;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        text.text = upgrade.itemName;
        image.sprite = upgrade.sprite;
        
        transform.localPosition = Vector3.Lerp(transform.localPosition, targetPos, lerpSpeed);
        
        animator.SetBool("Select", select);
    }
}
