﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroy : MonoBehaviour
{
    public static bool create;

    void Awake()
    {
        if (create)
        {
            Debug.Log("44");
            if (FindObjectOfType<LevelManager>())
            {
                FindObjectOfType<LevelManager>().CanvasValid();
            }
            Destroy(gameObject);
        }
        else
        {
            Debug.Log("88");
            
            create = true;
            DontDestroyOnLoad(this);
            
            FindObjectOfType<LevelManager>().CanvasValid();
        }
    }

    public void SetFalse()
    {
        create = false;
    }

    public bool CreateTest()
    {
        return create;
    }
}
