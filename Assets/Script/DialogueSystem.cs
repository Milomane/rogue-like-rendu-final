﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DialogueSystem : MonoBehaviour
{
    [TextArea]
    public List<string> text;

    public PnjDialogue pnjCorresponding;
    
    public float charPerSec;
    public int startBuble;

    public bool skipPressed;
    
    public TextMeshProUGUI textMeshPro;
    public TextMeshProUGUI textAdapter;

    public GameObject skipArrow;


    private void Start()
    {
        skipArrow.SetActive(false);
        textAdapter.text = "";
        
        string stringMaxWidht = "";
        int stringMaxHeight = 0;
        foreach (string s in text)
        {
            int countCharOnLine = 0;
            int countLine = 1;
            string tempString = "";
            foreach (char c in s)
            {
                tempString += c;
                if (c.ToString() == "\n")
                {
                    if (tempString.Length > stringMaxWidht.Length)
                    {
                        stringMaxWidht = tempString;
                    }

                    countLine++;
                    tempString = "";
                    
                }
                countCharOnLine++;
            }

            if (countLine == 0) countLine = 1;
            if (stringMaxHeight < countLine) stringMaxHeight = countLine;
            if (stringMaxWidht == "" && tempString.Length > stringMaxWidht.Length)
            {
                stringMaxWidht = tempString;
            }
        }
        
        for (int i = 0; i < stringMaxHeight; i++)
        {
            textAdapter.text += stringMaxWidht;
        }
        
        StartCoroutine(DrawDialogue(text, 1 / charPerSec, startBuble));
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) || Input.GetKeyDown(KeyCode.JoystickButton0))
        {
            skipPressed = true;
        }
    }

    public IEnumerator DrawDialogue(List<string> textToDraw, float timeBetweenChar, int startBubble = 0)
    {
        textMeshPro.text = "";
        
        foreach (char letter in textToDraw[startBubble])
        {
            textMeshPro.text += letter;
            
            if (skipPressed)
            {
                textMeshPro.text = textToDraw[startBubble];
                skipPressed = false;
                break;
            }
            
            yield return new WaitForSeconds(timeBetweenChar);
        }

        startBubble ++;
        skipArrow.SetActive(true);

        if (startBubble >= textToDraw.Count)
        {
            while (!skipPressed) {
                yield return new WaitForSeconds(Time.deltaTime);
            }
            
            skipPressed = false;
            skipArrow.SetActive(false);
            if (pnjCorresponding != null) pnjCorresponding.DialogueEnd();
            Destroy(gameObject);
        }
        else
        {
            while (!skipPressed) {
                yield return new WaitForSeconds(Time.deltaTime);
            }
            skipPressed = false;
            
            StartCoroutine(DrawDialogue(textToDraw, timeBetweenChar, startBubble));
        }
    }
}
