﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public AudioClip zoneMusic;
    public string roomName;

    void Update()
    {
        if (GameObject.FindGameObjectWithTag("RoomName"))
        {
            GameObject.FindGameObjectWithTag("RoomName").GetComponent<TextMeshProUGUI>().text = roomName;
        }
    }

    public IEnumerator LunchCinematique()
    {
        yield return new WaitForSeconds(.01f);
        StartCoroutine(FindObjectOfType<FadeIn>().CinematiqueStart());
    }

    public void CanvasValid()
    {
        StartCoroutine(LunchCinematique());
    }
}