﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeneralClass
{
    [System.Serializable]
    public class WeaponUpgrade
    {
        public int attack, speed, range, knockback, angle, stuntime;
    }
    
    [System.Serializable]
    public class PlayerUpgrade
    {
        public int speed, health, dashDistance;
    }

    [System.Serializable]
    public class ShopItem
    {
        public Upgrade upgrade;
        public int minPrice;
        public int maxPrice;
    }

    [System.Serializable]
    public class DestructibleObjectPoolA
    {
        public ScriptableDestructibleObject destructibleObject;
        public float probability = 0.25f;
    }

    public enum RoomType
    {
        neutral,
        trap,
        treasure,
        buff
    }
    
    public enum RoomChanger
    {
        neutral,
        spawn,
        enemy,
        shop,
        boss
    }
    
    [System.Serializable] 
    public class OpenSide 
    { 
        public bool up; 
        public bool down; 
        public bool left; 
        public bool right; 
    } 
     
    public enum Dir 
    { 
        up, 
        down, 
        left, 
        right 
    }
}
