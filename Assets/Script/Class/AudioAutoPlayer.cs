﻿using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Rendering;

namespace AudioAutoPlayerSpace
{
    public class AudioAutoPlayer
    {
        public static void Play(AudioClip audioClip, Vector3 transformPos, AudioMixerGroup audioMixer = null, float volume = 1f, float pitch = 1f, int priority = 128)
        {
            GameObject prefabObject = (GameObject)Resources.Load("Prefab/SoundPlayer");
            GameObject obj = GameObject.Instantiate(prefabObject, transformPos, Quaternion.identity);
            obj.GetComponent<AudioScript>().PlayAudio(audioClip, volume, pitch, audioMixer, priority);
        }
    }
}