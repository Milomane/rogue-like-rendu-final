﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerChooseRoom : MonoBehaviour
{
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "PlayerFeet")
        {
            StartCoroutine(FindObjectOfType<Generator>().ShowUI());
            Destroy(gameObject);
        }
    }
}
