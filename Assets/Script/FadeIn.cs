﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FadeIn : MonoBehaviour
{
    public bool fadeIn = true;
    public float fadeTime = 3f;
    private Image fadeRenderer;
    public PlayerController _playerController;
    private PlayerCinematiqueMouvement _playerCinematiqueMouvement;

    private void Start()
    {
        fadeRenderer = GameObject.FindGameObjectWithTag("Fade").GetComponent<Image>();
        _playerController = FindObjectOfType<PlayerController>();
        _playerCinematiqueMouvement = FindObjectOfType<PlayerCinematiqueMouvement>();
    }

    public IEnumerator CinematiqueStart()
    {
        if (fadeIn)
        {
            fadeRenderer = GameObject.FindGameObjectWithTag("Fade").GetComponent<Image>();
            _playerController = FindObjectOfType<PlayerController>();
            _playerCinematiqueMouvement = FindObjectOfType<PlayerCinematiqueMouvement>();
            
            _playerController.playerAsControl = false;
            fadeRenderer.color = new Color(0,0,0,1);
            fadeRenderer.gameObject.GetComponentInChildren<TextMeshProUGUI>().color = new Color(1,1,1,1);
            
            
            yield return new WaitForSeconds(.7f);
            StartCoroutine(_playerCinematiqueMouvement.MovePlayer(_playerController.transform.position + new Vector3(0, 2f, 0), true));
            
            Debug.Log("Ici");
            
            yield return new WaitForSeconds(.3f);
            fadeRenderer.DOFade(0, fadeTime);
            fadeRenderer.gameObject.GetComponentInChildren<TextMeshProUGUI>().DOFade(0, fadeTime+2);
            
            yield return new WaitForSeconds(fadeTime);
        }
    }
}
