﻿using System;
using System.Collections;
using System.Collections.Generic;
using GeneralClass;
using UnityEngine;
using UnityEngine.Scripting; 

public class RoomManager : MonoBehaviour
{
    [Header("Room requierement")] 
    public DungeonDoorsVar dungeonDoors;
    public Wave[] allWave;
    public int maxWaveCount;
    
    public GameObject shop;
    public BossSpawner bossSpawner;
    public Transform spawnPosition;
    public GameObject trap;
    public GameObject item;
    

    [Header("Type and changer")]
    public RoomType roomType;
    public RoomChanger roomChanger;

    [Header("Special")]
    
    public bool specialEnterCondition;
    public OpenSide specialCloseSide;

    [Header("Doors")]
    public OpenSide canBeOpenSide; 
    public OpenSide openSide;
    public DoorSystem doorSystem; 
    
    private bool enter;
    private PlayerController playerController;
    
    [System.Serializable] 
    public class Wave
    {
        public EnemySpawner[] spawners;
    }
    
    [System.Serializable] 
    public class DoorSystem 
    { 
        public DoorPart leftPart; 
        public DoorPart rightPart; 
        public DoorPart upPart; 
        public DoorPart downPart; 
    } 
    [System.Serializable] 
    public class DungeonDoorsVar
    { 
        public DungeonDoor left; 
        public DungeonDoor right; 
        public DungeonDoor up; 
        public DungeonDoor down; 
    } 
     
    [System.Serializable]  
    public class DoorPart 
    { 
        public GameObject door; 
        public GameObject noDoor; 
    } 

    public void Start()
    {
        if (shop != null) shop.SetActive(false);
        if (item != null) item.SetActive(false);
        if (trap != null) trap.SetActive(false);
        
        GenerateRoom();
        DoorControl(true);
        if (specialCloseSide.down || specialCloseSide.up || specialCloseSide.left || specialCloseSide.right)
        {
            SpecialDoorControl();
        }
    }
    
    public void Update() 
    { 
        doorSystem.downPart.door.SetActive(canBeOpenSide.down && openSide.down); 
        doorSystem.upPart.door.SetActive(canBeOpenSide.up && openSide.up); 
        doorSystem.leftPart.door.SetActive(canBeOpenSide.left && openSide.left); 
        doorSystem.rightPart.door.SetActive(canBeOpenSide.right && openSide.right); 
         
        doorSystem.downPart.noDoor.SetActive(!(canBeOpenSide.down && openSide.down)); 
        doorSystem.upPart.noDoor.SetActive(!(canBeOpenSide.up && openSide.up)); 
        doorSystem.leftPart.noDoor.SetActive(!(canBeOpenSide.left && openSide.left)); 
        doorSystem.rightPart.noDoor.SetActive(!(canBeOpenSide.right && openSide.right)); 
    }

    // Action append when player enter the stage
    public void GenerateRoom()
    {
        switch (roomChanger)
        {
            case RoomChanger.neutral :
                // Do nothing
                break;
            case RoomChanger.spawn :
                // Spawn player here
                FindObjectOfType<PlayerController>().transform.position = spawnPosition.position;
                
                break;
            case RoomChanger.enemy :
                // Wait for player to enter room
                break;
            case RoomChanger.boss :
                // Wait for player to enter room
                break;
            case RoomChanger.shop :
                // Spawn shop
                shop.SetActive(true);
                
                break;
        }

        switch (roomType)
        {
            case RoomType.neutral:
                // Do nothing
                break;
            case RoomType.trap:
                trap.SetActive(true);
                // Spawn trap
                break;
            case RoomType.buff:
                // Wait for player to enter room
                break;
            case RoomType.treasure:
                item.SetActive(true);
                // Spawn treasure
                break;
        }
    }
    
    // Action append when player enter the room
    public void EnterRoom()
    {
        if (!specialEnterCondition)
        {
            if (!enter)
            {
                enter = true;
        
                switch (roomChanger)
                {
                    case RoomChanger.enemy :
                        // Start spawn enemy wave coroutine, close door
                        DoorControl(false);
                        StartCoroutine(SpawnEnemyWave(maxWaveCount));
                        break;
                    case RoomChanger.boss :
                        // Spawn boss, close door
                        DoorControl(false);
                        StartCoroutine(SpawnBoss());
                        break;
                }

                switch (roomType)
                {
                    case RoomType.buff :
                        playerController.playerBuff = true;
                        break;
                }
            }
        }
    }

    public IEnumerator SpawnEnemyWave(int maxWave)
    {
        if (maxWave > allWave.Length) maxWave = allWave.Length;
        
        // Loop in all needed wave
        for (int i = 0; i < maxWave; i++)
        {
            List<GameObject> enemies = new List<GameObject>();
            
            // Spawn 1 wave
            foreach (var spawner in allWave[i].spawners)
            {
                enemies.Add(spawner.Spawn());
            }
            
            // Check all ennemies are dead
            while (enemies.Count > 0)
            {
                for (int y = 0; y < enemies.Count; y++)
                {
                    if (!enemies[y])
                    {
                        enemies.RemoveAt(y);
                        y--;
                    }
                }
                
                yield return new WaitForSeconds(Time.deltaTime);
            }
        }

        yield return null;
        
        DoorControl(true);
    }

    public IEnumerator SpawnBoss()
    {
        GameObject boss = bossSpawner.Spawn();

        while (boss)
        {
            yield return new WaitForSeconds(Time.deltaTime);
        }
        
        DoorControl(true);
    }

    // Control door opening
    public void DoorControl(bool open)
    {
        dungeonDoors.down.open = open;
        dungeonDoors.up.open = open;
        dungeonDoors.left.open = open;
        dungeonDoors.right.open = open;
    }

    public void SpecialDoorControl()
    {
        dungeonDoors.down.open = !specialCloseSide.down;
        dungeonDoors.up.open = !specialCloseSide.up;
        dungeonDoors.left.open = !specialCloseSide.left;
        dungeonDoors.right.open = !specialCloseSide.right;
    }

    public void SpecialStartCondition()
    {
        specialEnterCondition = false;
        EnterRoom();
    }
}
