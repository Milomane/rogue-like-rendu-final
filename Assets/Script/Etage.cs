﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Etage : MonoBehaviour
{
    public bool spawnPlayerSpecificPos;
    public GameObject player, canvas;
    public Transform spawnPos;
    
    private void Start()
    {
        
        player = GameObject.Find("Player");
        canvas = GameObject.Find("Camera and canvas");

        if (spawnPos != null)
        {
            player.transform.position = spawnPos.position;
            canvas.transform.position = spawnPos.position;
        }
    }
}
