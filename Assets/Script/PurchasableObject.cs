﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PurchasableObject : MonoBehaviour
{
    public ItemPool itemPool;
    public bool isRandomItem;
    
    public int price;
    
    public Upgrade upgrade;
    public SpriteRenderer spriteRenderer;
    public float outlineThickness = 3.5f;
    public bool showInfo;

    public GameObject infoUI;
    
    public GameObject dialogueNoMoney;
    public PnjDialogue shopKeeperDialogue;
    
    public TextMeshProUGUI textInfo;
    public TextMeshProUGUI textInfoFitter;
    public TextMeshProUGUI textPriceInfo;
    public TextMeshProUGUI textPriceInfoFitter;
    public Color colorNoMoney;
    public Color colorBase;

    private float timeBeforeClose;
    
    void Start()
    {
        if (isRandomItem)
        {
            int itemIndex = Random.Range(0, itemPool.pool.Count - 1);
            int startIndex = itemIndex;
            while (FindObjectOfType<ItemPoolManager>().alreadyUseUpgrade.Contains(itemPool.pool[itemIndex]))
            {
                itemIndex++;
                if (itemIndex >= itemPool.pool.Count)
                {
                    itemIndex = 0;
                }

                if (itemIndex == startIndex)
                {
                    FindObjectOfType<ItemPoolManager>().ResetUseItem();
                }
            }
            FindObjectOfType<ItemPoolManager>().AddItemToUse(itemPool.pool[itemIndex]);
            upgrade = itemPool.pool[itemIndex].upgrade;
            price = Random.Range(itemPool.pool[itemIndex].minPrice, itemPool.pool[itemIndex].maxPrice);
        }
        
        spriteRenderer.sprite = upgrade.sprite;
        colorBase = textPriceInfo.color;
        
        // Set text info
        string info = "";

        if (upgrade.weaponUpgrade.angle != 0 || upgrade.weaponUpgrade.attack != 0 ||
            upgrade.weaponUpgrade.knockback != 0 || upgrade.weaponUpgrade.speed != 0 ||
            upgrade.weaponUpgrade.range != 0 || upgrade.weaponUpgrade.stuntime != 0)
        {
            info += "Arme :\n";

            info += AddStatInfoText(upgrade.weaponUpgrade.attack, "<sprite=0>");
            info += AddStatInfoText(upgrade.weaponUpgrade.range, "<sprite=2>");
            info += AddStatInfoText(upgrade.weaponUpgrade.angle, "<sprite=4>");
            info += AddStatInfoText(upgrade.weaponUpgrade.knockback, "<sprite=3>");
            info += AddStatInfoText(upgrade.weaponUpgrade.speed, "<sprite=1>");
            info += AddStatInfoText(upgrade.weaponUpgrade.stuntime, "<sprite=5>");
        }

        if (upgrade.playerUpgrade.health != 0 || upgrade.playerUpgrade.speed != 0 || 
            upgrade.playerUpgrade.dashDistance != 0)
        {
            info += "Joueur :\n";

            info += AddStatInfoText(upgrade.playerUpgrade.health, "<sprite=8>");
            info += AddStatInfoText(upgrade.playerUpgrade.speed, "<sprite=6>");
            info += AddStatInfoText(upgrade.playerUpgrade.dashDistance, "<sprite=7>");
        }
        
        textInfo.text = info;
        textInfoFitter.text = info;

        textPriceInfo.text = price + "  -";
        textPriceInfoFitter.text = price + "  -";
    }

    public string AddStatInfoText(int value, string name)
    {
        string statInfo = "";
        
        if (value != 0)
        {
            statInfo += name + " ";
            if (value > 0) statInfo += "+";
            statInfo += value + "\n";
        }

        return statInfo;
    }

    public void Update()
    {
        // Detect if player is not selecting object
        if (timeBeforeClose > 0) timeBeforeClose -= Time.deltaTime;
        else showInfo = false;
        
        // Show info of object
        if (showInfo)
        {
            spriteRenderer.material.SetFloat("_OutlineThickness", outlineThickness);
            infoUI.SetActive(true);
        }
        else
        {
            spriteRenderer.material.SetFloat("_OutlineThickness", 0);
            infoUI.SetActive(false);
        }

        if (FindObjectOfType<PlayerController>().money < price)
        {
            textPriceInfo.color = colorNoMoney;
        }
        else
        {
            textPriceInfo.color = colorBase;
        }
    }

    public void Interact()
    {
        if (FindObjectOfType<PlayerController>().money >= price)
        {
            FindObjectOfType<PlayerController>().money -= price;
            
            FindObjectOfType<PlayerController>().AddUpgrade(upgrade);
            FindObjectOfType<NewItemPanelScript>().TriggerPanel(upgrade);
            Destroy(gameObject);
        }
        else
        {
            shopKeeperDialogue.ForceDialogue(dialogueNoMoney);
        }
    }

    public void Detect()
    {
        timeBeforeClose = Time.deltaTime*2;
        showInfo = true;
    }
}
