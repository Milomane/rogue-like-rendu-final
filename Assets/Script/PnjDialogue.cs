﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PnjDialogue : MonoBehaviour
{
    
    public GameObject prefabDialogue;

    public Transform dialoguePos;
    public float dialogueTimeOut;
    private GameObject currentDialogueObject;
    
    public float outlineThickness = 3.5f;
    public bool showInfo;
    
    [Header("Call special function")]
    public GameObject targetGo;
    public string functionName;

    private SpriteRenderer spriteRenderer;
    private Animator animator;
    private float timeBeforeClose;
    private float timeBeforeCloseDialogue;
    
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        if (currentDialogueObject == null)
        {
            timeBeforeCloseDialogue = 0;
        }

        // Detect if player is not selecting object
        if (timeBeforeClose > 0) timeBeforeClose -= Time.deltaTime;
        else showInfo = false;
        
        // Detect if player is next to the pnj
        if (timeBeforeCloseDialogue > 0)
        {
            timeBeforeCloseDialogue -= Time.deltaTime;
        }
        else
        {
            if (currentDialogueObject != null) Destroy(currentDialogueObject);
            currentDialogueObject = null;
        }
        
        // Show info of object
        if (showInfo)
        {
            spriteRenderer.material.SetFloat("_OutlineThickness", outlineThickness);
        }
        else
        {
            spriteRenderer.material.SetFloat("_OutlineThickness", 0);
        }
    }

    public void ForceDialogue(GameObject forcePrefabDialogue)
    {
        if (currentDialogueObject != null)
        {
            Destroy(currentDialogueObject);
        }
        currentDialogueObject = Instantiate(forcePrefabDialogue, dialoguePos);
        timeBeforeCloseDialogue = dialogueTimeOut;
    }
    
    public void Interact()
    {
        if (currentDialogueObject == null)
        {
            currentDialogueObject = Instantiate(prefabDialogue, dialoguePos);
            currentDialogueObject.GetComponent<DialogueSystem>().pnjCorresponding = this;
            timeBeforeCloseDialogue = dialogueTimeOut;
        }
    }
    
    public void Detect()
    {
        timeBeforeClose = Time.deltaTime*2;
        showInfo = true;

        if (timeBeforeCloseDialogue > 0) timeBeforeCloseDialogue = dialogueTimeOut;
    }
    
    
    public void DialogueEnd()
    {
        if (targetGo != null && functionName != null) targetGo.SendMessage(functionName);
    }
}
