﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PNJDynamiqueLayer : MonoBehaviour
{
    private SpriteRenderer _spriteRenderer;
    private Transform _player;
    public float offsetY;
    
    void Start()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _player = FindObjectOfType<PlayerController>().transform;
    }
    void Update()
    {
        if (_player.position.y < transform.position.y + offsetY)
        {
            _spriteRenderer.sortingOrder = -20;
        }
        else
        {
            _spriteRenderer.sortingOrder = 20;
        }
    }
}
