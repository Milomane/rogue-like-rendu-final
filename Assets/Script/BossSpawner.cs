﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossSpawner : MonoBehaviour
{
    public GameObject bossPrefab;
    
    
    void Start()
    {
        
    }
    void Update()
    {
        
    }

    public GameObject Spawn()
    {
        GameObject b = Instantiate(bossPrefab, transform.position, Quaternion.identity);
        return b;
    }
}
