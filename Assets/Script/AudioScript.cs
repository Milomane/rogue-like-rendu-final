﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;
using UnityEngine;

public class AudioScript : MonoBehaviour
{
    private AudioSource audioSource;
    public AudioMixerGroup audioMixerGroup;

    public void PlayAudio(AudioClip audioClip, float volume = 1f, float pitch = 1f, AudioMixerGroup audioMixer = null, int priority = 128)
    {
        audioSource = GetComponent<AudioSource>();
        audioMixerGroup = audioMixer;
        
        //FindObjectOfType<AudioManager>().AddNewSound(gameObject.GetComponent<AudioScript>(), audioClip);
        
        audioSource.clip = audioClip;
        audioSource.volume = volume;
        audioSource.pitch = pitch;
        audioSource.outputAudioMixerGroup = audioMixer;
        audioSource.priority = priority;
        
        audioSource.Play();
        Destroy(gameObject, audioClip.length);
    }
}
