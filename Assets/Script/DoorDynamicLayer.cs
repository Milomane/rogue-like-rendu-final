﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorDynamicLayer : MonoBehaviour
{
    private GameObject player;
    private SpriteRenderer spriteRenderer;

    private bool started;
    
    void Start()
    {
        StartCoroutine(TimeLate());
    }
    
    void Update()
    {
        if (started)
        {
            if (player.transform.position.y < transform.position.y + 1.28f)
            {
                spriteRenderer.sortingOrder = -20;
            }
            else
            {
                spriteRenderer.sortingOrder = 20;
            }
        }
    }

    public IEnumerator TimeLate()
    {
        yield return new WaitForSeconds(.05f);
        player = FindObjectOfType<PlayerController>().gameObject;
        spriteRenderer = GetComponent<SpriteRenderer>();
        started = true;
    }
}
