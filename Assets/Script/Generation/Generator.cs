﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using GeneralClass;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class Generator : MonoBehaviour
{
    [Header("Stage properties")]
    public int stageWidth;
    public int stageHeight;

    public int roomTileHeight;
    public int roomTileWidth;

    public float cellSizeMultiplier = 1.28f;
    public Vector2 roomSizeMultiplier;

    public int roomNumberMin;
    public int roomNumberMax;
    
    public AllPool allPool;

    [Header("Types probalities")]
    public float trapProb;
    public float treasureProb;
    public float buffProb;

    public float treasureProbMultiplier;

    [Header("Changers probalities")] 
    public float enemyProb;
    public float shopProb;
    
    [Header("Info")]
    public GameObject prefabGeneratorTile;
    public Vector2 spawnPos;
    public TextMeshProUGUI text;
    public GameObject spawnRoomPrefab;
    private InputManager inputManager;
    
    private bool editingStage;
    private Vector2 coordFirstTile;

    [Header("UI - Camera - Player")]
    public GameObject generatorUI;
    public GameObject validateButton;
    public CameraPivotScript cameraPivotScript;
    public PlayerController playerController;
    public PlayerCinematiqueMouvement playerCinematique;

    [Header("Arrays")]
    public List<RoomTypeLine> roomTypeArray;

    public List<RoomProperties> roomChoosingList;
    public List<RoomChanger> roomChangerList;

    private AllPool allPoolBuffer; 
    private int counter = 0;
    private RoomProperties lastRoomProperties;

    [System.Serializable]
    public class RoomTypeLine
    {
        public List<RoomProperties> roomProperties;
    }

    [System.Serializable]
    public class RoomProperties
    {
        public RoomType roomType;
        public RoomChanger roomChanger;

        public GeneratorTile tileCorresponding;
        public RoomManager roomManager;
        public OpenSide openSide;
    }

    [System.Serializable]
    public class AllPool 
    { 
        public RoomPool up; 
        public RoomPool down; 
        public RoomPool left; 
        public RoomPool right; 
    }

    void Start()
    {
        playerController = FindObjectOfType<PlayerController>();
        playerCinematique = FindObjectOfType<PlayerCinematiqueMouvement>();
        inputManager = FindObjectOfType<InputManager>();
        Debug.Log(playerCinematique);
        
        cameraPivotScript = FindObjectOfType<CameraPivotScript>();
        
        // Add all pull to buffer 
        allPoolBuffer = allPool;
        
        // Calc const of room size
        roomSizeMultiplier = new Vector2(roomTileWidth * cellSizeMultiplier, roomTileHeight * cellSizeMultiplier);
        
        // Choose random room type
        roomTypeArray = new List<RoomTypeLine>(stageHeight);
        for (int y = 0; y < stageHeight; y++)
        {
            roomTypeArray.Add(new RoomTypeLine());
            roomTypeArray[y].roomProperties = new List<RoomProperties>();
            
            for (int i = 0; i < stageWidth; i++)
            {
                roomTypeArray[y].roomProperties.Add(new RoomProperties());

                roomTypeArray[y].roomProperties[i].tileCorresponding =
                    Instantiate(prefabGeneratorTile, transform.position, Quaternion.identity).GetComponent<GeneratorTile>();
                roomTypeArray[y].roomProperties[i].openSide = new OpenSide();
                
                
                // Test for spawn, else choose random room
                if (y == 0 && i == stageWidth / 2)
                {
                    roomTypeArray[y].roomProperties[i].roomType = RoomType.neutral;
                    roomTypeArray[y].roomProperties[i].roomChanger = RoomChanger.spawn;
                    roomTypeArray[y].roomProperties[i].roomManager = Instantiate(spawnRoomPrefab, new Vector2(i, y) * roomSizeMultiplier, Quaternion.identity).GetComponent<RoomManager>();
                    roomTypeArray[y].roomProperties[i].roomManager.DoorControl(false);
                    spawnPos = new Vector2(i, y);
                }
                else
                {
                    float r = Random.Range(0f, 1f);
                    if (r <= trapProb) roomTypeArray[y].roomProperties[i].roomType = RoomType.trap;
                
                    else if (r > trapProb && r <= trapProb + treasureProb) 
                        roomTypeArray[y].roomProperties[i].roomType = RoomType.treasure;
                
                    else if (r > trapProb + treasureProb && r <= trapProb + treasureProb + buffProb)
                        roomTypeArray[y].roomProperties[i].roomType = RoomType.buff;
                
                    else roomTypeArray[y].roomProperties[i].roomType = RoomType.neutral;
                }
            }

            treasureProb = treasureProb * treasureProbMultiplier;
        }
        
        // Replace all generator tile
        for (int y = 0; y < stageHeight; y++)
        {
            for (int i = 0; i < stageWidth; i++)
            {
                roomTypeArray[y].roomProperties[i].tileCorresponding.coordinate = new Vector2(y, i);
                roomTypeArray[y].roomProperties[i].tileCorresponding.tileType = roomTypeArray[y].roomProperties[i].roomType;
                roomTypeArray[y].roomProperties[i].tileCorresponding.generator = this;

                roomTypeArray[y].roomProperties[i].tileCorresponding.transform.position =
                    spawnPos * roomSizeMultiplier 
                    + (new Vector2(i, y) 
                    - new Vector2(stageWidth/2f, stageHeight/2f) 
                    + new Vector2(.5f, .5f)) * cellSizeMultiplier;
            }
        }
        
        lastRoomProperties = roomTypeArray[(int) spawnPos.x].roomProperties[(int) spawnPos.y];
        
        // Choose random list of changers
        // Add spawn
        roomChangerList.Add(RoomChanger.spawn);
        
        // Add intern room
        int roomN = Random.Range(roomNumberMin, roomNumberMax)-2;
        for (int i = 0; i < roomN; i++)
        {
            float r = Random.Range(0f, 1f);
            if (r <= enemyProb)
            {
                roomChangerList.Add(RoomChanger.enemy);
            }
            else if (r > enemyProb && r <= enemyProb + shopProb)
            {
                roomChangerList.Add(RoomChanger.shop);
            }
            else
            {
                roomChangerList.Add(RoomChanger.neutral);
            }
        }
        
        // Add boss
        roomChangerList.Add(RoomChanger.boss);

        text.text = "";
        foreach (var roomChanger in roomChangerList)
        {
            switch (roomChanger)
            {
                case RoomChanger.boss :
                    text.text += "Boss" + "\n";
                    break;
                case RoomChanger.enemy :
                    text.text += "Ennemis" + "\n";
                    break;
                case RoomChanger.neutral :
                    text.text += "Neutre" + "\n";
                    break;
                case RoomChanger.shop :
                    text.text += "Magasin" + "\n";
                    break;
                case RoomChanger.spawn :
                    text.text += "Spawn" + "\n";
                    break;
            }
            
        }
    }
    void Update()
    {
        if (editingStage)
        {
            text.text = "";

            for (int i = counter+1; i < roomChangerList.Count; i++)
            {
                switch (roomChangerList[i])
                {
                    case RoomChanger.boss :
                        text.text += "Boss" + "\n";
                        break;
                    case RoomChanger.enemy :
                        text.text += "Ennemis" + "\n";
                        break;
                    case RoomChanger.neutral :
                        text.text += "Neutre" + "\n";
                        break;
                    case RoomChanger.shop :
                        text.text += "Magasin" + "\n";
                        break;
                    case RoomChanger.spawn :
                        text.text += "Spawn" + "\n";
                        break;
                }
            }

            if (counter + 1 < roomChangerList.Count)
            {
                validateButton.SetActive(false);
                
                if (inputManager.GetButtonDown("Up"))
                {
                    if (playerController.transform.position.y + cellSizeMultiplier < roomTypeArray[roomTypeArray.Count - 1].roomProperties[0].tileCorresponding.transform.position.y + .5f)
                    {
                        StartCoroutine(MovePlayer(Vector2.up * (cellSizeMultiplier + .1f)));
                    }
                } else if (inputManager.GetButtonDown("Down"))
                {
                    if (playerController.transform.position.y - cellSizeMultiplier > roomTypeArray[0].roomProperties[0].tileCorresponding.transform.position.y - .5f)
                    {
                        StartCoroutine(MovePlayer(Vector2.down * (cellSizeMultiplier + .1f)));
                    }
                } else if (inputManager.GetButtonDown("Left"))
                {
                    if (playerController.transform.position.x - cellSizeMultiplier > roomTypeArray[0].roomProperties[0].tileCorresponding.transform.position.x - .5f)
                    {
                        StartCoroutine(MovePlayer(Vector2.left * (cellSizeMultiplier + .1f)));
                    }
                } else if (inputManager.GetButtonDown("Right"))
                {
                    if (playerController.transform.position.x + cellSizeMultiplier < roomTypeArray[0].roomProperties[roomTypeArray[0].roomProperties.Count-1].tileCorresponding.transform.position.x + .5f)
                    {
                        StartCoroutine(MovePlayer(Vector2.right * (cellSizeMultiplier + .1f)));
                    }
                }
            }
            else
            {
                validateButton.SetActive(true);
            }
        }
    }

    public IEnumerator ChooseRoom(GeneratorTile gt)
    {
        Debug.Log("Choose room");
        
        if (!gt.use)
        {
            gt.use = true;
        
            // Coordinate (Add the propertie to the list)
            if (roomChoosingList.Count < 1)
            {
                roomChoosingList = new List<RoomProperties>();
            }
            else
            {
                counter++;
            }
            
            roomChoosingList.Add(roomTypeArray[(int)gt.coordinate.x].roomProperties[(int)gt.coordinate.y]);
            Debug.Log("Room type : " + roomChoosingList[counter].roomType + "    original : " + roomTypeArray[(int)gt.coordinate.x].roomProperties[(int)gt.coordinate.y].roomType);

            // Changer
            roomChoosingList[counter].roomChanger = roomChangerList[counter];
        
            yield return new WaitForSeconds(.05f);
            
            // Opening
            Debug.Log("RoomChoosing list count : " + roomChoosingList.Count);
            if (roomChoosingList.Count > 1)
            {
                Vector2 tempValue = roomChoosingList[counter].tileCorresponding.coordinate -
                                    lastRoomProperties.tileCorresponding.coordinate;
                
                Debug.Log(tempValue);
                
                switch (tempValue.y)
                {
                    case 1 : // Right
                        roomChoosingList[counter].openSide.left = true;
                        lastRoomProperties.openSide.right = true;
                        break;
                    case -1 : // Left
                        roomChoosingList[counter].openSide.right = true;
                        lastRoomProperties.openSide.left = true;
                        break;
                }

                switch (tempValue.x)
                {
                    case 1 : // Up
                        roomChoosingList[counter].openSide.down = true;
                        lastRoomProperties.openSide.up = true;
                        break;
                    case -1 : // Down
                        roomChoosingList[counter].openSide.up = true;
                        lastRoomProperties.openSide.down = true;
                        break;
                }
            }
        }
        
        lastRoomProperties = roomTypeArray[(int)gt.coordinate.x].roomProperties[(int)gt.coordinate.y];
    }

    public void Validate() 
    { 
        // Generate the dungeon with the roomChoosingList 
        Debug.Log("Validate");
        
        if (counter+1 == roomChangerList.Count)
        {
            Debug.Log("Enter in the boucle");
            

            roomChoosingList[0].roomManager.openSide = roomChoosingList[0].openSide;

            for (int i = 1; i < roomChoosingList.Count; i++)
            {
                Vector2 spawnCoord = new Vector2(roomChoosingList[i].tileCorresponding.coordinate.y * roomSizeMultiplier.y, roomChoosingList[i].tileCorresponding.coordinate.x * roomSizeMultiplier.x); ;
                Debug.Log(i + "    spawn coordinate : " + roomChoosingList[i].tileCorresponding.coordinate);
                GameObject roomChooseObject = ChooseRoom(roomChoosingList[i].openSide);

                roomChoosingList[i].roomManager = Instantiate(roomChooseObject, spawnCoord, Quaternion.identity).GetComponent<RoomManager>();

                roomChoosingList[i].roomManager.roomChanger = roomChoosingList[i].roomChanger;
                roomChoosingList[i].roomManager.roomType = roomChoosingList[i].roomType;
                roomChoosingList[i].roomManager.openSide = roomChoosingList[i].openSide;
            }

            StartCoroutine(ValidateUI());
        }
        else
        {
            if (counter + 1 > roomChangerList.Count)
            {
                Debug.Log("TROP DE ROOM");
            }
            else
            {
                Debug.Log("PAS ASSEZ");
            }
        }
    }

    public IEnumerator ValidateUI()
    {
        generatorUI.GetComponent<GeneratorUIVar>().mask.DOFade(0, 2);
        Camera.main.GetComponent<CameraController>().Shake(2, .1f, 10, 45);
        
        foreach (var roomLines in roomTypeArray)
        {
            foreach (var roomPropertie in roomLines.roomProperties)
            {
                if (!roomPropertie.tileCorresponding.use) roomPropertie.tileCorresponding.gameObject.SetActive(false);
            }
        }
        
        yield return new WaitForSeconds(2);
        generatorUI.SetActive(false);
        playerController.playerAsControl = true;
        cameraPivotScript.TargetPlayer();
    }

    public void ResetRoom()
    {
        playerController.transform.position = coordFirstTile;
        roomChoosingList = new List<RoomProperties>();
        lastRoomProperties = roomTypeArray[(int)spawnPos.x].roomProperties[(int)spawnPos.y];
        counter = 0;
        
        foreach (var roomLines in roomTypeArray)
        {
            foreach (var roomPropertie in roomLines.roomProperties)
            {
                roomPropertie.openSide.down = false;
                roomPropertie.openSide.up = false;
                roomPropertie.openSide.left = false;
                roomPropertie.openSide.right = false;

                roomPropertie.roomChanger = RoomChanger.neutral;
                roomPropertie.tileCorresponding.use = false;
            }
        }
    }
    
    public GameObject ChooseRoom(OpenSide openSide) 
    { 
        GameObject roomObject = new GameObject(); 
        bool ok = false; 
 
        // Choose random between all side 
        int randN = Random.Range(0, 3); 
        while (!ok) 
        { 
            switch (randN) 
            { 
                case 0 : 
                    if (openSide.up) 
                    { 
                        bool okk = false; 
                        int randM = Random.Range(0, allPoolBuffer.up.pool.Count - 1);

                        int testCount = 0;
                        while (!okk) 
                        { 
                            // Test if all side of 1 room is corresponding to the open side
                            if (TestAllSide(openSide, allPoolBuffer.up.pool[randM]))
                            {
                                roomObject = allPoolBuffer.up.pool[randM];
                                okk = true;
                                ok = true;
                            } 
                            else 
                            { 
                                randM++; 
                                if (randM >= allPoolBuffer.up.pool.Count) randM = 0;
                                
                                testCount++;
                                if (testCount >= allPoolBuffer.up.pool.Count)
                                {
                                    testCount = 0;
                                    allPoolBuffer.up = allPool.up;
                                }
                            }
                        } 
                    } 
                    else randN++; 
                    break; 
                case 1: 
                    if (openSide.down) 
                    { 
                        bool okk = false; 
                        int randM = Random.Range(0, allPoolBuffer.down.pool.Count - 1);

                        int testCount = 0;
                        while (!okk) 
                        { 
                            // Test if all side of 1 room is corresponding to the open side
                            if (TestAllSide(openSide, allPoolBuffer.down.pool[randM]))
                            {
                                roomObject = allPoolBuffer.down.pool[randM];
                                okk = true;
                                ok = true;
                            } 
                            else 
                            { 
                                randM++; 
                                if (randM >= allPoolBuffer.down.pool.Count) randM = 0;
                                
                                testCount++;
                                if (testCount >= allPoolBuffer.down.pool.Count)
                                {
                                    testCount = 0;
                                    allPoolBuffer.down = allPool.down;
                                }
                            }
                        } 
                    } 
                    else randN++; 
                    break; 
                case 2:
                    if (openSide.left) 
                    { 
                        bool okk = false; 
                        int randM = Random.Range(0, allPoolBuffer.left.pool.Count - 1);

                        int testCount = 0;
                        while (!okk) 
                        { 
                            // Test if all side of 1 room is corresponding to the open side
                            if (TestAllSide(openSide, allPoolBuffer.left.pool[randM]))
                            {
                                roomObject = allPoolBuffer.left.pool[randM];
                                okk = true;
                                ok = true;
                            } 
                            else 
                            { 
                                randM++; 
                                if (randM >= allPoolBuffer.left.pool.Count) randM = 0;
                                
                                testCount++;
                                if (testCount >= allPoolBuffer.left.pool.Count)
                                {
                                    testCount = 0;
                                    allPoolBuffer.left = allPool.left;
                                }
                            }
                        } 
                    } 
                    else randN++; 
                    break; 
                case 3:
                    if (openSide.right) 
                    { 
                        bool okk = false; 
                        int randM = Random.Range(0, allPoolBuffer.right.pool.Count - 1);

                        int testCount = 0;
                        while (!okk) 
                        { 
                            // Test if all side of 1 room is corresponding to the open side
                            if (TestAllSide(openSide, allPoolBuffer.right.pool[randM]))
                            {
                                roomObject = allPoolBuffer.right.pool[randM];
                                okk = true;
                                ok = true;
                            } 
                            else 
                            { 
                                randM++; 
                                if (randM >= allPoolBuffer.right.pool.Count) randM = 0;
                                
                                testCount++;
                                if (testCount >= allPoolBuffer.right.pool.Count)
                                {
                                    testCount = 0;
                                    allPoolBuffer.right = allPool.right;
                                }
                            }
                        } 
                    } 
                    else randN++; 
                    break; 
            }

            if (randN > 3) randN = 0;
        } 
 
        // If any of this side are ok with all other, take it 
        // else -> +1 loop 
 
        // else refill 
        return roomObject; 
    } 
 
    public bool TestAllSide(OpenSide openSide, GameObject room) 
    { 
        bool valid = true; 
         
        if (openSide.up) 
        { 
            if (!allPool.up.pool.Contains(room)) valid = false; 
        } 
 
        if (openSide.down) 
        { 
            if (!allPool.down.pool.Contains(room)) valid = false; 
        } 
 
        if (openSide.left) 
        { 
            if (!allPool.left.pool.Contains(room)) valid = false; 
        } 
 
        if (openSide.right) 
        { 
            if (!allPool.right.pool.Contains(room)) valid = false; 
        } 
 
        return valid; 
    }

    public IEnumerator ShowUI()
    {
        generatorUI.SetActive(true);
        cameraPivotScript.SpecificTarget(roomTypeArray[(int)spawnPos.y].roomProperties[(int)spawnPos.x].roomManager.gameObject);
        generatorUI.GetComponent<GeneratorUIVar>().mask.color = new Color(1,1,1,0);
        generatorUI.GetComponent<GeneratorUIVar>().mask.DOFade(1, 1);
        
        playerController.playerAsControl = false;
        coordFirstTile =
            (Vector2) roomTypeArray[(int) spawnPos.y].roomProperties[(int) spawnPos.x].roomManager.gameObject.transform.position - new Vector2(0, cellSizeMultiplier * 2 - .3f);
        StartCoroutine(playerCinematique.MovePlayer(coordFirstTile, false));
        
        editingStage = true;
        yield return new WaitForSeconds(1f);
    }

    public IEnumerator MovePlayer(Vector2 movementVector)
    {
        StartCoroutine(playerCinematique.MovePlayer((Vector2)playerController.transform.position + movementVector, false));
        editingStage = false;
        yield return new WaitForSeconds(1.2f);
        editingStage = true;
    }
}
