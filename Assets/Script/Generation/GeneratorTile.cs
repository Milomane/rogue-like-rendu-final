﻿using System;
using System.Collections;
using System.Collections.Generic;
using GeneralClass;
using UnityEngine;

public class GeneratorTile : MonoBehaviour
{
    public RoomType tileType;
    public Vector2 coordinate;

    public bool use;
    public GameObject validator;

    private Animator animator;
    public Generator generator;
    
    void Start()
    {
        animator = GetComponent<Animator>();
    }
    
    void Update()
    {
        switch (tileType)
        {
            case RoomType.neutral:
                animator.SetInteger("State", 0);
                break;
            case RoomType.trap:
                animator.SetInteger("State", 1);
                break;
            case RoomType.treasure:
                animator.SetInteger("State", 2);
                break;
            case RoomType.buff:
                animator.SetInteger("State", 3);
                break;
        }
        
        validator.SetActive(use);
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "PlayerFeet")
        {
            StartCoroutine(generator.ChooseRoom(this));
        }
    }
}
