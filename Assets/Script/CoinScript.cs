﻿using System;
using System.Collections;
using System.Collections.Generic;
using AudioAutoPlayerSpace;
using UnityEngine;
using UnityEngine.Audio;
using Random = UnityEngine.Random;

public class CoinScript : MonoBehaviour
{
    public int amount;
    private Rigidbody2D rb;
    
    public float maxPushForce;
    public float maxRotationForce;

    public AudioClip pickupCoinClip;
    public AudioMixerGroup mixer;
    
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        
        rb.velocity = new Vector2(Random.Range(-maxPushForce, maxPushForce), Random.Range(-maxPushForce, maxPushForce));
        rb.angularVelocity = Random.Range(-maxRotationForce, maxRotationForce);
    }
    
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<PlayerController>())
        {
            other.GetComponent<PlayerController>().money += amount;
            AudioAutoPlayer.Play(pickupCoinClip, transform.position, mixer, .7f);
            
            Destroy(gameObject);
        }
    }
}
