﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasVar : MonoBehaviour
{
    public GameObject bossBar, victoryPanel;
    public Transform bossBarScaler;
    public Image bossBarImage;
}
